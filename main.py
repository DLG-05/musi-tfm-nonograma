import cv2

from algoritmos.backtracking_with_ai_v3 import BacktrackingVorazConIAV3
from algoritmos.modos_reflexiones import ModoReflexiones
from vision.picross5 import PicrossS5, ModoDetection

ps5 = PicrossS5(modo=ModoDetection.AUTO)
nonograma = None

INDEX_CAPTURADORA = 1

video = cv2.VideoCapture(INDEX_CAPTURADORA + cv2.CAP_DSHOW)

video.set(3, 1280)
video.set(4, 720)

width = video.get(3)
height = video.get(4)
aspect = float(width) / float(height)
print("Dimension:" + str(width) + "x" + str(height))
print("Aspect:" + str(aspect))

frame_bin = None

while video.isOpened():
    ret, frame = video.read()

    frame_c, es_juego = ps5.es_pantalla_de_juego(frame)

    if es_juego:
        if nonograma is None:
            nonograma, frame_bin = ps5.obtener_cabeceras_nonograma(frame)
            cv2.imshow('frame2', frame_bin)
            print(nonograma)
            algoritmo = BacktrackingVorazConIAV3(nonograma, ia_enabled=True, intuition=True, using_prediction=True,
                                                 check_nn_prediction=True, enable_prints=True,
                                                 reflexiones=ModoReflexiones.COMPLETO)
            algoritmo.start()
    else:
        nonograma = None

    cv2.imshow('frame', frame_c)
    k = cv2.waitKey(1) & 0xff
    if k == 13:
        cv2.imwrite('frame.png', frame_bin)
    if k == 27:  # Si se pulsa ESC acabar
        break

video.release()
cv2.destroyAllWindows()
