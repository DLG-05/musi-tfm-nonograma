# NonoIA: Solucionar nonogramas mediante redes neuronales y visión por ordenador

Máster Universitario. Sistemas Inteligentes por la Universidad de las Islas Baleares

**Autor**: David López

Un nonograma es un puzle lógico donde se intenta generar una imagen a partir de las restricciones indicadas en sus cabeceras. En este proyecto se propondrán diferentes técnicas para resolver este tipo de problema lógico utilizando una combinación de redes neuronales con algoritmos basados en *backtracking* y reglas lógicas (heurístico) o algoritmos genéticos. Además de realizar un proceso de captación de nonogramas mediante el videojuego PicrossS5 en la Nintendo Switch.

## Requisitos

* Una Nintendo Switch (normal) con el PicrossS5
* Una Capturadora HDMI compatible con OpenCV
* Gráfica NVIDIA RTX o similar con compatibilidad con Tensorflow para entrenar las redes neuronales.
* 16 GB de memoria RAM

## Instalación

El código propuesto está implementado mediante Python y requiere tener una distribución de **Ananconda 3** instalada en el sistema.

Además, se proporciona el entorno de trabajo utilizando Windows 10.

Para instalar el entorno de trabajo se deben realizar los siguientes pasos:

```bash
conda env create --file env.yml
conda activate tfm_david
jupyter labextension install @jupyter-widgets/jupyterlab-manager
jupyter nbextension enable --py widgetsnbextension
jupyter lab build
python -m ipykernel install --user --name tfm_david --display-name "Py3 (TFM David)"
```

> **Nota**: El conjunto de datos y los modelos entrenados no se proporcionan en este repositorio, se deben generar manualmente y se recomienda generar una carpeta para los datos ```.\<proyecto>\datos\<tamaño del tablero>``` y ```.\<proyecto>\modelos``` y sin esta información el código no funcionará.

## Ejecución

Si se dispone de los modelos entrenados y del conjunto de datos, además de los requisitos mencionados, la ejecución se realiza mediante el siguiente comando:

```bash
conda activate tfm_david
python main.py
```

> **Nota**: Es posible que el índice de la capturadora se deba cambiar al que utilice tu dispositivo.
>
> **Nota 2**: Si se usa alguna de las utilidades de la carpeta ```utilidades``` es posible que se tenga que modificar el path de inicio del código para que detecte los paquetes que se han implementado.
