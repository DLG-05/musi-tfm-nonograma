import time
from threading import Thread

import numpy as np
from estructura import Nonograma, Tipo, FilaColumna, Contenido


class BacktrackingVoraz(Thread):

    def __init__(self, tablero: Nonograma) -> None:
        super().__init__()
        self.__tablero = tablero

    def run(self):
        inicio = time.monotonic()
        valor, tablero_new = self.algoritmo(self.__tablero)
        print(tablero_new)
        final = time.monotonic() - inicio
        print(round(final, 4))

    def algoritmo(self, tablero):
        filas_columnas_pendientes = tablero.get_numero_filas_columnas_no_rellenadas()
        if filas_columnas_pendientes == 0 and tablero.is_solution():
            return True, tablero
        elif filas_columnas_pendientes == 0:
            return False, None

        combinaciones = self.get_combinaciones(tablero)
        # Si la cabeza me indica que no hay combinaciones
        if combinaciones[0][3] == 0:
            return False, None

        combination_mas_probable = combinaciones[0]
        for combination in combination_mas_probable[2]:
            tablero_c = tablero.copiar()
            tablero_c.set_fila_columna_visitada(combination_mas_probable[0], combination_mas_probable[1])
            tablero_c.set_fila_columna_valor(combination_mas_probable[0], combination_mas_probable[1], combination)
            resuelto, tablero_resuelto = self.algoritmo(tablero_c)
            if resuelto:
                return resuelto, tablero_resuelto
        return False, None

    def get_combinaciones(self, tablero: Nonograma):
        combinaciones_filas = self.get_combinations_fila_columna(tablero, Tipo.FILA, tablero.get_ancho_tablero())
        combinaciones_columnas = self.get_combinations_fila_columna(tablero, Tipo.COLUMNA, tablero.get_alto_tablero())
        combinaciones = [*combinaciones_filas, *combinaciones_columnas]
        combinaciones_ordenadas = sorted(combinaciones, key=lambda x: x[3])
        return combinaciones_ordenadas

    def get_combinations_fila_columna(self, tablero: Nonograma, tipo: Tipo, maximum: int):
        if tipo == Tipo.FILA:
            a_visitar = tablero.get_filas_no_rellenadas()
        else:
            a_visitar = tablero.get_columnas_no_rellenadas()
        combinaciones = []
        for idx in a_visitar:
            combinaciones_fila_columna = [tipo, idx, [], 0]
            fila_columna = tablero.get_cabecera_fila_columna(tipo, idx)
            if fila_columna.comprobar_si_es_trivial(maximum):
                if fila_columna.get_len_lista() != 0:
                    espacios = np.ones(fila_columna.get_len_lista(), dtype=np.int32)
                    espacios[0] = 0
                    combinaciones_fila_columna[2].append(self.convertir_a_vector(tipo, espacios, fila_columna, tablero))
                else:
                    if tipo == Tipo.FILA:
                        rango = tablero.get_ancho_tablero()
                    else:
                        rango = tablero.get_alto_tablero()
                    vector = np.zeros(rango, dtype=Contenido)
                    vector.fill(Contenido.CROSS)
                    combinaciones_fila_columna[2].append(vector)
            else:
                # la parte superior no tiene espacios en la primera combinación
                # la combinación con la que se empieza es esta |0|1|...|1|
                espacios = np.ones(fila_columna.get_len_lista(), dtype=np.int32)
                espacios[0] = 0
                hay_carry = False
                while not hay_carry:
                    if espacios.sum() + fila_columna.get_valores().sum() <= maximum:
                        vector = self.convertir_a_vector(tipo, espacios, fila_columna, tablero)
                        if self.comprobar_combination(tipo, idx, vector, tablero):
                            combinaciones_fila_columna[2].append(self.convertir_a_vector(tipo, espacios, fila_columna,
                                                                                         tablero))
                    espacios, hay_carry = self.incrementar_vector(espacios, maximum)
            combinaciones_fila_columna[3] = len(combinaciones_fila_columna[2])
            combinaciones.append(combinaciones_fila_columna)
        return combinaciones

    @staticmethod
    def comprobar_combination(tipo: Tipo, idx_fila_columna, combination_vector, tablero: Nonograma):
        fil_col_or = tablero.get_fila_columna_valor(tipo, idx_fila_columna)
        fila_columna_generada = FilaColumna.generar_fila_columna(combination_vector)
        if fila_columna_generada.get_len_lista() != fila_columna_generada.get_len_lista():
            return False
        for valor_or, valor_new in zip(fil_col_or, combination_vector):
            if valor_or == Contenido.CROSS and valor_new == Contenido.FULL:
                return False
            if valor_or == Contenido.FULL and valor_new == Contenido.CROSS:
                return False
        return True

    @staticmethod
    def convertir_a_vector(tipo: Tipo, espacios, fila_columna: FilaColumna, tablero: Nonograma):
        if tipo == Tipo.FILA:
            vector = np.zeros(tablero.get_ancho_tablero(), dtype=Contenido)
            vector.fill(Contenido.CROSS)
        else:
            vector = np.zeros(tablero.get_alto_tablero(), dtype=Contenido)
            vector.fill(Contenido.CROSS)

        contador = 0
        for espacio, valor in zip(espacios, fila_columna.get_valores()):
            contador += espacio
            for i in range(valor):
                vector[contador] = Contenido.FULL
                contador += 1
        return vector

    @staticmethod
    def incrementar_vector(espacios, valor_maximo):
        """
        Se encarga de incrementar un vector como un contador led
        Basado en la implementación de:
        # https://www.geeksforgeeks.org/adding-one-to-number-represented-as-array-of-digits/
        Realizadas modificaciones para tener en cuenta que el 0 no es un valor válido.
        :param espacios:
        :param valor_maximo:
        :return:
        """
        a = espacios.copy()
        n = len(a)

        # Add 1 to last digit and find carry
        a[n - 1] += 1
        carry = a[n - 1] // valor_maximo
        a[n - 1] = a[n - 1] % valor_maximo

        # evita que el array pierda el espacio en las componentes de en medio
        if a[n - 1] == 0:
            a[n - 1] = 1

        # Traverse from second last digit
        for i in range(n - 2, -1, -1):
            if carry == 1:
                a[i] += 1
                carry = a[i] // valor_maximo
                a[i] = a[i] % valor_maximo
                # Reinicia el vector cuando hay carry a 1
                if a[i] == 0:
                    a[i] = 1
        if carry == 1:
            return a, True
        else:
            return a, False
