import numpy as np

from algoritmos.modos_reflexiones import ModoReflexiones
from deeplearning_utils.utilidades import cargar_modelo
from tensorflow.keras.backend import clear_session

from estructura import Nonograma, Contenido, FilaColumna, Tipo

PATH_OF_MODELS = './modelos/'
SELECTED_MODEL_5x5 = 7
SELECTED_MODEL_10x10 = 19
SELECTED_MODEL_15x15 = 19
NAME_MODEL_5x5 = "modelo_"
NAME_MODEL_10x10 = "modelo10x10_"
NAME_MODEL_15x15 = 'modelo15x15_'


def cargar_modelos(tablero, path=PATH_OF_MODELS, m_5x5=SELECTED_MODEL_5x5, m_10x10=SELECTED_MODEL_10x10,
                   m_15x15=SELECTED_MODEL_15x15, name_m_15x15=NAME_MODEL_15x15,
                   name_m_5x5=NAME_MODEL_5x5, name_m_10x10=NAME_MODEL_10x10,):
    if tablero.get_ancho_tablero() == 5 and tablero.get_alto_tablero() == 5:
        return cargar_modelo(m_5x5, path, name_models=name_m_5x5)
    elif tablero.get_ancho_tablero() == 10 and tablero.get_alto_tablero() == 10:
        return cargar_modelo(m_10x10, path, name_models=name_m_10x10)
    elif tablero.get_ancho_tablero() == 15 and tablero.get_alto_tablero() == 15:
        return cargar_modelo(m_15x15, path, name_models=name_m_15x15)
    else:
        raise ValueError("No hay modelo de DL entrenado para este tipo de tablero")


def cargar_modelo_manual(size, path=PATH_OF_MODELS, m_5x5=SELECTED_MODEL_5x5, m_10x10=SELECTED_MODEL_10x10,
                         m_15x15=SELECTED_MODEL_15x15, name_m_15x15=NAME_MODEL_15x15,
                         name_m_5x5=NAME_MODEL_5x5, name_m_10x10=NAME_MODEL_10x10):
    if size == 5:
        return cargar_modelo(m_5x5, path, name_models=name_m_5x5)
    elif size == 10:
        return cargar_modelo(m_10x10, path, name_models=name_m_10x10)
    elif size == 15:
        return cargar_modelo(m_15x15, path, name_models=name_m_15x15)
    else:
        raise ValueError("No hay modelo de DL entrenado para este tipo de tablero")


def predecir_usando_ia(modelo, tablero: Nonograma):
    """
    Sistema que predice un tablero mediante inteligencia artificial
    :param modelo: Modelo cargado para el tablero concreto
    :param tablero: tablero
    :return: Tablero predicho, las estructuras internas se mantendrán idénticas a las
    """
    cabeceras = tablero.get_cabeceras_con_padding_y_concatenadas()
    # Se agrega una dimensión para que pueda predecir
    cabeceras_adaptadas = np.expand_dims(cabeceras, axis=0)
    # Para predecir una muestra hay que evitar el predict
    # https://stackoverflow.com/questions/66271988/warningtensorflow11-out-of-the-last-11-calls-to-triggered-tf-function-retracin
    nuevo_tablero = modelo(cabeceras_adaptadas).numpy()
    clear_session()
    size_of_tablero = (tablero.get_alto_tablero(), tablero.get_ancho_tablero())
    # El tablero predicho se redondea, se convierte a enteros y se cambia el tamaño
    tablero_int = nuevo_tablero[0].round().astype(np.uint8).reshape(size_of_tablero)
    # Se cambia el tipo a Contenido
    tablero_contenido = Nonograma.KEYS[tablero_int].astype(Contenido)
    # Se asigna a un nuevo tablero a partir del original
    nuevo_nonograma = tablero.copiar_asignando_nuevo_tablero_reset_pendientes(tablero_contenido)
    return nuevo_nonograma


def aplicar_transformaciones_correspondientes(predicciones, idx, size_of_tablero):
    tablero_adaptado = predicciones[idx].round().astype(np.uint8).reshape(size_of_tablero)
    if idx == 0:  # Identidad
        return tablero_adaptado
    elif idx == 1:  # cf
        return np.transpose(tablero_adaptado)
    elif idx == 2:  # fC
        return np.flip(tablero_adaptado, axis=1)
    elif idx == 3:  # Fc
        return np.flip(tablero_adaptado, axis=0)
    elif idx == 4:  # cF
        return np.flip(np.transpose(tablero_adaptado), axis=0)
    elif idx == 5:  # Cf
        return np.flip(np.transpose(tablero_adaptado), axis=1)
    elif idx == 6:  # FC
        return np.flip(np.flip(tablero_adaptado, axis=0), axis=1)
    elif idx == 7:  # CF
        return np.flip(np.flip(np.transpose(tablero_adaptado), axis=0), axis=1)


def predecir_usando_ia_transformaciones(modelo, tablero: Nonograma, transformaciones: ModoReflexiones):
    """
    Sistema que predice un tablero mediante inteligencia artificial
    :param transformaciones: Modo de trabajo para las cabeceras
    :param modelo: Modelo cargado para el tablero concreto
    :param tablero: tablero
    :return: Tablero predicho, las estructuras internas se mantendrán idénticas a las
    """
    if transformaciones == ModoReflexiones.SOLO2:
        cabeceras = tablero.get_cabeceras_con_padding_y_concatenadas()
        cabeceras_inversas = tablero.get_cabeceras_con_padding_y_concatenadas_invertidas()
        lista_cabeceras = np.array([cabeceras, cabeceras_inversas])
    elif transformaciones == ModoReflexiones.COMPLETO:
        lista_cabeceras = tablero.get_8_transformaciones()
    else:
        raise ValueError("Modo introducido incorrecto")
    # Para predecir una muestra hay que evitar el predict
    # https://stackoverflow.com/questions/66271988/warningtensorflow11-out-of-the-last-11-calls-to-triggered-tf-function-retracin
    nuevo_tablero = modelo(lista_cabeceras).numpy()
    clear_session()
    size_of_tablero = (tablero.get_alto_tablero(), tablero.get_ancho_tablero())

    nonogramas = []
    celdas_guardadas = np.zeros(len(nuevo_tablero))
    for i in range(len(nuevo_tablero)):
        # El tablero predicho se redondea, se convierte a enteros y se cambia el tamaño y se aplican las
        # transformaciones
        tablero1 = aplicar_transformaciones_correspondientes(nuevo_tablero, i, size_of_tablero)

        tablero_contenido1 = Nonograma.KEYS[tablero1].astype(Contenido)
        nonograma1 = tablero.copiar_asignando_nuevo_tablero_reset_pendientes(tablero_contenido1)
        dic1 = nonograma1.calcular_cabeceras_equivocadas()
        celdas1 = (tablero.get_alto_tablero() - len(dic1[Tipo.FILA])) * (tablero.get_ancho_tablero() -
                                                                         len(dic1[Tipo.COLUMNA]))
        nonogramas.append(nonograma1)
        celdas_guardadas[i] = celdas1

    tablero_seleccionado = int(np.argmax(celdas_guardadas))
    return nonogramas[tablero_seleccionado]


def convertir_row_of_dataset_into_nonograma(x, ancho, alto):
    x_conv = x.reshape((ancho+alto, -1))

    n = Nonograma(ancho=ancho, alto=alto)

    for i in range(alto):
        lista_valores_adaptados = []
        lista_valores_originales = x_conv[i]
        for valor in lista_valores_originales:
            if valor != 0:
                lista_valores_adaptados.append(valor)
        n.agregar(Tipo.FILA, i, FilaColumna(lista_valores_adaptados))

    for i in range(ancho):
        lista_valores_adaptados = []
        lista_valores_originales = x_conv[i+alto]
        for valor in lista_valores_originales:
            if valor != 0:
                lista_valores_adaptados.append(valor)
        n.agregar(Tipo.COLUMNA, i, FilaColumna(lista_valores_adaptados))
    n.calcular_cuadros_a_marcar()
    return n
