from enum import IntEnum


class ModoReflexiones(IntEnum):
    DESACTIVADO = 0
    SOLO2 = 1
    COMPLETO = 2
