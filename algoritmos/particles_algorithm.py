import time
from threading import Thread

from tqdm import tqdm

from algoritmos.particles_structure import ParticlesStructure
from estructura import Nonograma
import random


class ParticleAlgorithm(Thread):

    def __init__(self, nonograma: Nonograma, size_of_population, iteraciones, use_second=False, use_test=False,
                 enable_prints=False, enable_tqdm=False) -> None:
        super().__init__()
        self.__estructura = ParticlesStructure(nonograma, size_of_population, use_second_function=use_second)
        self.__size_of_population = size_of_population
        self.__iteraciones = iteraciones
        self.__enable_prints = enable_prints
        self.__use_test = use_test
        self.__enable_tqdm = enable_tqdm

        self.ia_a_la_primera = False
        self.time = 0
        self.hay_solution = False
        self.solution = None
        self.iteration = 0

    def run(self):
        inicio = time.monotonic()
        carga = self.__estructura.generar_population_inicial(use_test=self.__use_test)
        if carga is not None:
            self.time = time.monotonic() - inicio
            self.ia_a_la_primera = True
            self.hay_solution = True
            self.solution = carga
            if self.__enable_prints:
                print(carga)
            return
        if self.__enable_tqdm:
            it = tqdm(range(self.__iteraciones))
        else:
            it = range(self.__iteraciones)
        for idx in it:
            peso_total = self.__estructura.get_peso_total()
            for i in range(self.__size_of_population):
                w = random.random() * peso_total  # Se escoge un número aleatorio entre 0 y peso total
                # Se escoge un individuo que cumple la condición que w <= PesoAcum
                individuo_x = self.__estructura.get_individuo(w)
                individuo_y = individuo_x.copiar()  # Se genera una copia de X
                # Se obtiene el número de mutaciones que debería realizar y se aplican
                mutations = self.__estructura.get_num_mutations(individuo_y)
                for mutation in range(mutations):
                    individuo_y.mutation()
                # Una vez aplicadas se guarda el elemento y si es la solución se para el algoritmo
                self.__estructura.set_new_element_of_population(individuo_y, i)
                if individuo_y.is_solution():
                    self.time = time.monotonic() - inicio
                    self.solution = individuo_y
                    self.iteration = idx+1
                    self.hay_solution = True
                    if self.__enable_prints:
                        print(individuo_y)
                        print(time.monotonic() - inicio)
                    return
            self.__estructura.change_population()

        self.time = time.monotonic() - inicio
        self.solution = None
        self.iteration = self.__iteraciones
        self.hay_solution = False
        if self.__enable_prints:
            print("Solución no encontrada")
            print(time.monotonic() - inicio)
