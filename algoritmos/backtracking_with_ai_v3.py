import time
from threading import Thread
from typing import Optional

import numpy as np

from algoritmos.modos_reflexiones import ModoReflexiones
from algoritmos.utilidades_ia import cargar_modelos, predecir_usando_ia, predecir_usando_ia_transformaciones
from estructura import Nonograma, Tipo, FilaColumna, Contenido


class BacktrackingVorazConIAV3(Thread):

    def __init__(self, tablero: Nonograma, ia_enabled=True, intuition=True, using_prediction=True,
                 check_nn_prediction=True, enable_prints=True,
                 reflexiones=ModoReflexiones.DESACTIVADO) -> None:
        super().__init__()
        self.__tablero = tablero
        self.iteraciones = 0

        self.__ia_enabled = ia_enabled
        self.__reflexiones = reflexiones
        self.__intuition = intuition
        self.__using_prediction = using_prediction
        self.__check_nn_prediction = check_nn_prediction
        if ia_enabled:
            self.__modelo = cargar_modelos(self.__tablero)
        self.__tablero_predicho_con_ml: Optional[Nonograma] = None
        self.use_backtracking = False
        self.time = 0
        self.__enable_prints = enable_prints
        self.info_backtracking = []
        self.solution: Optional[Nonograma] = None
        self.ia_a_la_primera = False
        self.filas_columnas_borradas = 0
        self.filas_borradas = 0
        self.celdas_borradas = 0
        self.celdas_mantenidas = 0
        self.columnas_borradas = 0
        self.borrado_completo = False
        self.veces_backtracking = 0

        self.veces_backtracking_pre_borrado_total = 0
        self.veces_backtracking_post_borrado_total = 0
        self.iteraciones_pre_borrado = 0
        self.iteraciones_post_borrado = 0

    def predecir_utilizando_modelo_dl(self):
        """
        Método que se encarga de predecir el tablero mediante un modelo de DL
        :return: Tablero predicho, puede que las casillas no sean correctas
        """
        # Posible incorporación
        if self.__reflexiones != ModoReflexiones.DESACTIVADO and self.__ia_enabled:
            return predecir_usando_ia_transformaciones(self.__modelo, self.__tablero, self.__reflexiones)
        elif self.__ia_enabled:
            return predecir_usando_ia(self.__modelo, self.__tablero)
        else:
            return self.__tablero

    def run(self):
        inicio = time.monotonic()
        self.__tablero_predicho_con_ml = self.predecir_utilizando_modelo_dl()
        if self.__ia_enabled and self.__check_nn_prediction and self.__tablero_predicho_con_ml.is_solution():
            self.time = time.monotonic() - inicio
            self.solution = self.__tablero_predicho_con_ml
            self.ia_a_la_primera = True
            if self.__enable_prints:
                print(self.__tablero_predicho_con_ml)
                print(round(self.time, 4))
        elif self.__ia_enabled and self.__using_prediction:
            tablero_adaptado = self.__tablero_predicho_con_ml.copiar()
            dic = tablero_adaptado.convertir_tablero_con_cabeceras_equivocadas()
            valor, tablero_resuelto = self.algoritmo(tablero_adaptado)
            if not valor:
                self.borrado_completo = True
                self.veces_backtracking_pre_borrado_total = self.veces_backtracking
                self.veces_backtracking = 0
                self.iteraciones_pre_borrado = self.iteraciones
                self.iteraciones = 0
                valor, tablero_resuelto = self.algoritmo(self.__tablero)
                self.time = time.monotonic() - inicio
                self.veces_backtracking_post_borrado_total = self.veces_backtracking
                self.iteraciones_post_borrado = self.iteraciones
            else:
                self.time = time.monotonic() - inicio
                self.veces_backtracking_pre_borrado_total = self.veces_backtracking
                self.veces_backtracking = 0
                self.iteraciones_pre_borrado = self.iteraciones
                self.iteraciones = 0
            self.solution = tablero_resuelto
            self.filas_columnas_borradas = dic['total']
            self.celdas_borradas = dic['celdas_borradas']
            self.celdas_mantenidas = dic['celdas_rellenadas']
            self.filas_borradas = len(dic[Tipo.FILA])
            self.columnas_borradas = len(dic[Tipo.COLUMNA])
            if self.__enable_prints:
                print(tablero_resuelto)
                print(round(self.time, 4))
                print(self.iteraciones)
                print(self.use_backtracking)
        else:
            valor, tablero_new = self.algoritmo(self.__tablero)
            self.time = time.monotonic() - inicio
            self.solution = tablero_new
            if self.__enable_prints:
                print(tablero_new)
                print(round(self.time, 4))
                print(self.iteraciones)
                print(self.use_backtracking)

    def algoritmo(self, tablero):
        self.iteraciones += 1
        filas_columnas_pendientes = tablero.get_numero_filas_columnas_no_rellenadas()
        if filas_columnas_pendientes == 0 and tablero.is_solution():
            return True, tablero
        elif filas_columnas_pendientes == 0:
            return False, []

        combinaciones, celdas_fijas = self.get_combinaciones(tablero)
        # Si la cabeza me indica que no hay combinaciones
        if combinaciones[0][3] == 0:
            return False, []

        # Se mira si hay celdas triviales y se marcan, en caso contrario se debe ir probando las combinaciones
        # utilizando siempre la que más posibilidades tiene de acertar.
        if len(celdas_fijas) != 0:
            tablero_c = tablero.copiar()
            for i, j, valor in celdas_fijas:
                tablero_c.cambiar_valor(i, j, valor)
            tablero_c.comprobar_si_las_casillas_tienen_valor()
            resuelto, tablero_resuelto = self.algoritmo(tablero_c)
            if resuelto:
                return resuelto, tablero_resuelto
        else:
            self.use_backtracking = True
            self.veces_backtracking += 1
            # tipo, idx, [], 0 -> indica el tipo, el índice de la fila o la columna, la lista de combinaciones válidas y
            # el tamaño de esa lista
            combination_mas_probable = combinaciones[0]
            # De la lista de las combinaciones más probables según el tablero que se tiene,
            # se mira si se trata de una fila o una columna y se extrae esa misma del tablero predicho por la red
            # neuronal, se calcula el peso mediante la función ```comprobar_si_red_neuronal_y_combination_coinciden```
            # y selecciona cada vez una combinación aleatoria dentro de la lista de valores más plausibles.
            if combination_mas_probable[0] == Tipo.FILA:
                fila_columna_seleccionada = self.__tablero_predicho_con_ml.get_tablero()[combination_mas_probable[1], :]
            else:  # combination_mas_probable[0] == Tipo.COLUMNA
                fila_columna_seleccionada = self.__tablero_predicho_con_ml.get_tablero()[:, combination_mas_probable[1]]

            combinaciones_valores = {}
            valores = []
            n_combinaciones = len(combination_mas_probable[2])
            for i, combination in enumerate(combination_mas_probable[2]):
                valor = self.comprobar_si_red_neuronal_y_combination_coinciden(fila_columna_seleccionada, combination,
                                                                               ia_enabled=self.__ia_enabled,
                                                                               intuition=self.__intuition)
                if valor not in combinaciones_valores:
                    combinaciones_valores[valor] = [combination]
                else:
                    combinaciones_valores[valor].append(combination)
                valores.append(valor)
            valores = sorted(valores, reverse=True)

            for i, valor in enumerate(valores):
                self.info_backtracking.append((valor, i, n_combinaciones, self.__ia_enabled))
                lista = combinaciones_valores[valor]
                idx_sel = np.random.randint(0, len(lista))
                combination = lista[idx_sel]
                combinaciones_valores[valor].pop(idx_sel)
                tablero_c = tablero.copiar()
                tablero_c.set_fila_columna_visitada(combination_mas_probable[0], combination_mas_probable[1])
                tablero_c.set_fila_columna_valor(combination_mas_probable[0], combination_mas_probable[1], combination)
                resuelto, tablero_resuelto = self.algoritmo(tablero_c)
                if resuelto:
                    return resuelto, tablero_resuelto

        # Si no encuentra ninguna combinación viable, retornar a un nivel anterior
        return False, []

    @staticmethod
    def comprobar_si_red_neuronal_y_combination_coinciden(array_red, array_combination, ia_enabled=True,
                                                          intuition=True):
        """
        Método que se encarga de ponderar la solución dada una fila o columna predicha por la red neuronal
        En caso de que no se tenga la red neuronal activada dará siempre el valor máximo que es el de la
        longitud del array.
        :param intuition: Si se hará el cálculo mediante la ponderación de la red
        :param array_red: Fila/columna predicha por la red
        :param array_combination: Fila/columna que se quiere aplicar
        :param ia_enabled: Si la red neuronal ha sido activada
        :return:
        """
        longitud = len(array_red)
        valor = longitud
        if ia_enabled and intuition:
            for i in range(longitud):
                red = array_red[i]
                comb = array_combination[i]
                # La red proporciona como información segura el contenido vacío y rellenado y el algoritmo de
                # backtracking recibe como información segura rellenado y cruz, el empty en este algoritmo significa
                # desconocido

                # Por lo tanto, si la red dice que tiene que estar rellenado y el backtracking indica que hay una cruz,
                # no es una combinación válida, y también al contrario, si el backtracking indica que está rellenada
                # y la red dice que no.

                # El resto de casos son válidos.

                if red == Contenido.FULL and comb == Contenido.CROSS:
                    valor -= 1
                elif red == Contenido.CROSS and comb == Contenido.FULL:  # La red neuronal predice cruces
                    valor -= 1
        return valor

    def get_combinaciones(self, tablero: Nonograma):
        combinaciones_filas, fijos_filas = self.get_combinations_fila_columna(tablero, Tipo.FILA,
                                                                              tablero.get_ancho_tablero())
        combinaciones_columnas, fijos_columnas = self.get_combinations_fila_columna(tablero, Tipo.COLUMNA,
                                                                                    tablero.get_alto_tablero())
        combinaciones = [*combinaciones_filas, *combinaciones_columnas]
        combinaciones_ordenadas = sorted(combinaciones, key=lambda x: x[3])
        fijos = [*fijos_filas, *fijos_columnas]
        fijos = self.filtrar_celdas_fijas(tablero, fijos)
        return combinaciones_ordenadas, fijos

    @staticmethod
    def filtrar_celdas_fijas(tablero: Nonograma, fijos):
        """
        Método que se encarga de filtrar celdas que ya han sido previamente marcadas en un tablero
        :param tablero: Tablero actual
        :param fijos: Lista con una tupla indicando la posición y el valor que debe tener
        :return:
        """
        lista_filtrada = []
        for i, j, valor in fijos:
            if tablero.get_valor(i, j) == Contenido.EMPTY:
                lista_filtrada.append((i, j, valor))
        return lista_filtrada

    def get_combinations_fila_columna(self, tablero: Nonograma, tipo: Tipo, maximum: int):
        if tipo == Tipo.FILA:
            a_visitar = tablero.get_filas_no_rellenadas()
        else:
            a_visitar = tablero.get_columnas_no_rellenadas()
        combinaciones = []
        valores_fijos = []
        for idx in a_visitar:
            combinaciones_fila_columna = [tipo, idx, [], 0]
            fila_columna = tablero.get_cabecera_fila_columna(tipo, idx)
            if fila_columna.comprobar_si_es_trivial(maximum):
                if fila_columna.get_len_lista() != 0:
                    # Representa los espacios que hay entre una cabecera que tiene 2 elementos o más y se
                    # indica que la primera componente nunca tiene espacios, ya que sino no se puede considerar
                    # trivial
                    espacios = np.ones(fila_columna.get_len_lista(), dtype=np.int32)
                    espacios[0] = 0
                    vector = self.convertir_a_vector(tipo, espacios, fila_columna, tablero)
                    combinaciones_fila_columna[2].append(vector)
                    # Se generan los valores fijos para esta fila/columna trivial
                    valores_fijos_nuevos = self.convertir_a_vector_full_cross(vector, tipo, idx)
                    valores_fijos += valores_fijos_nuevos
                else:
                    if tipo == Tipo.FILA:
                        rango = tablero.get_ancho_tablero()
                    else:
                        rango = tablero.get_alto_tablero()
                    vector = np.zeros(rango, dtype=Contenido)
                    vector.fill(Contenido.CROSS)
                    combinaciones_fila_columna[2].append(vector)
                    # Se generan los valores fijos para esta fila/columna trivial
                    valores_fijos_nuevos = self.convertir_a_vector_full_cross(vector, tipo, idx)
                    valores_fijos += valores_fijos_nuevos
            else:
                # la parte superior no tiene espacios en la primera combinación
                # la combinación con la que se empieza es esta |0|1|...|1|
                espacios = np.ones(fila_columna.get_len_lista(), dtype=np.int32)
                espacios[0] = 0
                hay_carry = False
                while not hay_carry:
                    if espacios.sum() + fila_columna.get_valores().sum() <= maximum:
                        vector = self.convertir_a_vector(tipo, espacios, fila_columna, tablero)
                        if self.comprobar_combination(tipo, idx, vector, tablero):
                            combinaciones_fila_columna[2].append(self.convertir_a_vector(tipo, espacios, fila_columna,
                                                                                         tablero))
                    espacios, hay_carry = self.incrementar_vector(espacios, maximum)

                # Se convierten las combinaciones a una tabla para poder mirar si hay celdas triviales
                valores_fijos_nuevos = self.convertir_combinaciones_a_tabla(combinaciones_fila_columna[2], tipo, idx,
                                                                            tablero)
                valores_fijos += valores_fijos_nuevos  # Se unen al total

            combinaciones_fila_columna[3] = len(combinaciones_fila_columna[2])
            combinaciones.append(combinaciones_fila_columna)

        return combinaciones, valores_fijos

    @staticmethod
    def convertir_a_vector_full_cross(vector, tipo, idx):
        """
        Se encarga de convertir un vector a celdas triviales
        :param vector: vector a convertir
        :param tipo: Si se trata de una fila o columna
        :param idx: índice de trabajo
        :return:
        """
        valores_fijos = []
        for i, valor in enumerate(vector):
            if tipo == Tipo.FILA:
                valores_fijos.append((idx, i, valor))
            else:
                valores_fijos.append((i, idx, valor))
        return valores_fijos

    @staticmethod
    def convertir_combinaciones_a_tabla(combinaciones, tipo: Tipo, idx, tablero: Nonograma):
        """
        Se encarga de convertir la lista de combinaciones a 2 vectores indicando la probabilidad
        que tienen de rellenarse con una X o con █
        :param combinaciones: lista de combinaciones
        :param tipo: Si se trata de una fila o de una columna
        :param idx: índice de trabajo
        :param tablero: tablero actual
        :return:
        """
        num_combinaciones = len(combinaciones)
        if tipo == Tipo.FILA:
            dimension = tablero.get_ancho_tablero()
        else:
            dimension = tablero.get_alto_tablero()

        vector_full = np.zeros(dimension)
        vector_cross = np.zeros(dimension)

        valores_fijos = []

        for combination in combinaciones:
            for i, valor in enumerate(combination):
                if valor == Contenido.CROSS:
                    vector_cross[i] += 1
                elif valor == Contenido.FULL:
                    vector_full[i] += 1
        for i in range(dimension):
            if vector_full[i] == num_combinaciones:
                if tipo == Tipo.FILA:
                    valores_fijos.append((idx, i, Contenido.FULL))
                else:
                    valores_fijos.append((i, idx, Contenido.FULL))
            if vector_cross[i] == num_combinaciones:
                if tipo == Tipo.FILA:
                    valores_fijos.append((idx, i, Contenido.CROSS))
                else:
                    valores_fijos.append((i, idx, Contenido.CROSS))
        return valores_fijos

    @staticmethod
    def comprobar_combination(tipo: Tipo, idx_fila_columna, combination_vector, tablero: Nonograma):
        """
        Método que se encarga de comprobar si una combinación es viable con el tablero que se tiene
        actualmente
        :param tipo: Si es fila o columna
        :param idx_fila_columna: el índice de trabajo
        :param combination_vector: el vector generado
        :param tablero: el tablero
        :return:
        """
        fil_col_or = tablero.get_fila_columna_valor(tipo, idx_fila_columna)
        fila_columna_generada = FilaColumna.generar_fila_columna(combination_vector)
        if fila_columna_generada.get_len_lista() != fila_columna_generada.get_len_lista():
            return False
        # Si hay una combinación que rompe el tablero original se descarta
        # o si la longitud de la nueva combinación no es la misma que la de la fila o columna
        # que se debe rellenar
        for valor_or, valor_new in zip(fil_col_or, combination_vector):
            if valor_or == Contenido.CROSS and valor_new == Contenido.FULL:
                return False
            if valor_or == Contenido.FULL and valor_new == Contenido.CROSS:
                return False
        return True

    @staticmethod
    def convertir_a_vector(tipo: Tipo, espacios, fila_columna: FilaColumna, tablero: Nonograma):
        """
        Convierte una combinación representada por el vector espacios y su cabecera correspondiente
        :param tipo: Si se trata de fila o columna
        :param espacios: el vector donde se indica la combinación de espacios
        :param fila_columna: Fila o Columna a tratar
        :param tablero: Tablero actual
        :return:
        """
        # Se inicializa el vector a cruces
        if tipo == Tipo.FILA:
            vector = np.zeros(tablero.get_ancho_tablero(), dtype=Contenido)
            vector.fill(Contenido.CROSS)
        else:
            vector = np.zeros(tablero.get_alto_tablero(), dtype=Contenido)
            vector.fill(Contenido.CROSS)
        # Para los valores de filas columnas y espacios
        contador = 0
        for espacio, valor in zip(espacios, fila_columna.get_valores()):
            contador += espacio  # Se saltan los espacios
            for i in range(valor):  # se marcan las casillas a rellenadas
                vector[contador] = Contenido.FULL
                contador += 1
        return vector

    @staticmethod
    def incrementar_vector(espacios, valor_maximo):
        """
        Se encarga de incrementar un vector como un contador led
        Basado en la implementación de:
        # https://www.geeksforgeeks.org/adding-one-to-number-represented-as-array-of-digits/
        Realizadas modificaciones para tener en cuenta que el 0 no es un valor válido.
        :param espacios:
        :param valor_maximo:
        :return:
        """
        a = espacios.copy()
        n = len(a)

        # Add 1 to last digit and find carry
        a[n - 1] += 1
        carry = a[n - 1] // valor_maximo
        a[n - 1] = a[n - 1] % valor_maximo

        # evita que el array pierda el espacio en las componentes de en medio
        if a[n - 1] == 0:
            a[n - 1] = 1

        # Traverse from second last digit
        for i in range(n - 2, -1, -1):
            if carry == 1:
                a[i] += 1
                carry = a[i] // valor_maximo
                a[i] = a[i] % valor_maximo
                # Reinicia el vector cuando hay carry a 1
                if a[i] == 0:
                    a[i] = 1
        if carry == 1:
            return a, True
        else:
            return a, False
