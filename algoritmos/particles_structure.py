from algoritmos.utilidades_ia import cargar_modelos, predecir_usando_ia
from estructura import Nonograma, FilaColumna
import numpy as np


class ParticlesStructure:

    def __init__(self, nonograma: Nonograma, size_of_population, use_second_function=False) -> None:
        super().__init__()
        self.__nonograma = nonograma
        self.__dl_model = cargar_modelos(nonograma)
        self.__size_of_population = size_of_population
        self.__population = np.empty(size_of_population, dtype=Nonograma)
        self.__peso = np.zeros(size_of_population)
        self.__peso_acumulado = np.zeros(size_of_population)

        self.__new_population = np.empty(size_of_population, dtype=Nonograma)
        self.__new_peso = np.zeros(size_of_population)
        self.__new_peso_acumulado = np.zeros(size_of_population)
        self.__segundo_peso = use_second_function

    def get_individuo(self, peso):
        posiciones = np.where(self.__peso_acumulado >= peso)
        return self.__population[posiciones[0][0]]

    @staticmethod
    def test_funcionamiento():
        """
        Método que genera un tablero de prueba para poder comprobar el funcionamiento del algoritmo
        Es un tablero que no tiene una celda de manera correcta.
        :return:
        """
        tablero = np.ones(shape=(5, 5), dtype=np.uint8)
        tablero[0, 2] = 0
        tablero_n = Nonograma.KEYS[tablero]

        n = Nonograma(ancho=5, alto=5, tablero=tablero_n)
        filas, columnas = n.calcular_cabeceras_actuales()
        filas[0] = FilaColumna([5])
        columnas[2] = FilaColumna([5])
        n = Nonograma(ancho=5, alto=5, tablero=tablero_n, filas=filas, columnas=columnas)
        n.calcular_cuadros_a_marcar()
        return n

    def test_funcionamiento_2(self):
        na = Nonograma.generar_nonograma_aleatorio(5, 5, 0.5)
        na = self.__nonograma.copiar_asignando_nuevo_tablero(na.get_tablero())
        return na

    def generar_population_inicial(self, use_test=False):
        """
        Método que se encarga de generar la población inicial.
        Se encarga de predecir el primer elemento mediante una red neuronal y si no es un tablero solución
        se generan diferentes mutaciones.
        :return:
        """
        if use_test:
            self.__population[0] = self.test_funcionamiento_2()
            # print(self.__population[0])
        else:
            self.__population[0] = predecir_usando_ia(self.__dl_model, self.__nonograma)
            # print(self.__population[0])

        if self.__population[0].is_solution():
            return self.__population[0]
        if self.__segundo_peso:
            self.__peso[0] = self.__population[0].calcular_peso2()
        else:
            self.__peso[0] = self.__population[0].calcular_peso()

        for i in range(1, self.__size_of_population):
            mutations = self.get_num_mutations(self.__population[0])
            copia = self.__population[0].copiar()
            for mutation in range(mutations):
                copia.mutation()
            if self.__segundo_peso:
                self.__peso[i] = copia.calcular_peso2()
            else:
                self.__peso[i] = copia.calcular_peso()
            self.__population[i] = copia
        self.__peso_acumulado = np.cumsum(self.__peso)
        return None

    def set_new_element_of_population(self, nonograma, i):
        """
        Método que permite asignar un nuevo individuo a la población.
        Hace el cálculo del peso
        :param nonograma: tablero a asignar
        :param i: posición
        :return:
        """
        self.__new_population[i] = nonograma
        if self.__segundo_peso:
            self.__new_peso[i] = nonograma.calcular_peso2()
        else:
            self.__new_peso[i] = nonograma.calcular_peso()
        # print(nonograma)
        # print("Peso del tablero {}".format(self.__new_peso[i]))

    def change_population(self):
        """
        Método que se encarga de cambiar la población antigua por la nueva generada, y preparando la estructura
        para considerar una nueva población
        :return:
        """
        self.__population = self.__new_population
        self.__peso = self.__new_peso
        self.__peso_acumulado = self.__new_peso.cumsum()

        self.__new_population = np.empty(self.__size_of_population, dtype=Nonograma)
        self.__new_peso = np.zeros(self.__size_of_population)
        self.__new_peso_acumulado = np.zeros(self.__size_of_population)

    def get_peso_total(self):
        """
        Retorna el peso total acumulado con todos los individuos
        :return:
        """
        return self.__peso_acumulado[-1]

    @staticmethod
    def get_num_mutations(nonograma: Nonograma):
        """
        Método encargado de calcular el número de mutaciones que debe realizar
        un tablero.
        Se realiza mediante el cálculo de cuantas filas y columnas tiene equivocadas
        y se se aplica una distribución normal al número obtenido
        :param nonograma:
        :return:
        """
        return np.random.randint(0, 4, dtype=int)

        # num_columnas_mal = nonograma.get_numero_columnas_equivocadas()
        # num_filas_mal = nonograma.get_numero_filas_equivocadas()
        #
        # errores = num_filas_mal + num_columnas_mal
        # num_mut = np.random.normal(errores, 1)
        # if num_mut < 0:
        #     return 0
        # else:
        #     return int(num_mut)
