from enum import IntEnum


class Tipo(IntEnum):
    COLUMNA = 0
    FILA = 1


class Contenido(IntEnum):
    CROSS = 0
    FULL = 1
    EMPTY = 2

