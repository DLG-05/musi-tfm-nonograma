import cv2 as cv
import numpy as np

from estructura.filacolumna import FilaColumna
from estructura.nonograma_enums import *
import random


class Nonograma:
    KEYS = np.array([Contenido.CROSS, Contenido.FULL, Contenido.EMPTY])

    def __init__(self, ancho: int = None, alto: int = None, filas=None, columnas=None, tablero=None,
                 columnas_no_rellenadas=None, filas_no_rellenadas=None, cuadros_a_marcar=0) -> None:
        self.__ancho = ancho
        self.__alto = alto
        if tablero is None and filas is None and columnas is None:
            self.__tablero = np.zeros((alto, ancho), dtype=Contenido)  # [fila, columna]
            self.__tablero.fill(Contenido.EMPTY)
            self.__columnas = np.empty(ancho, dtype=object)  # Representará la cabecera superior
            self.__filas = np.empty(alto, dtype=object)  # Representará la cabecera izquierda
        elif tablero is not None and filas is None and columnas is None:
            self.__tablero = tablero
            self.__columnas = np.empty(ancho, dtype=object)  # Representará la cabecera superior
            self.__filas = np.empty(alto, dtype=object)  # Representará la cabecera izquierda
            self.generar_cabeceras()
        else:
            self.__tablero = tablero
            self.__columnas = columnas
            self.__filas = filas
        if columnas_no_rellenadas is None and filas_no_rellenadas is None:
            self.__columnas_no_rellenadas = {}
            self.__filas_no_rellenadas = {}
            for i in range(self.__alto):
                self.__filas_no_rellenadas[i] = True
            for i in range(self.__ancho):
                self.__columnas_no_rellenadas[i] = True
        else:
            self.__columnas_no_rellenadas = columnas_no_rellenadas
            self.__filas_no_rellenadas = filas_no_rellenadas
            self.__cuadrados_a_marcar = cuadros_a_marcar

    def calcular_cuadros_a_marcar(self):
        self.__cuadrados_a_marcar = 0
        for columna in self.__columnas:
            cuadrados = int(columna.get_valores().sum())
            self.__cuadrados_a_marcar += cuadrados
        self.__cuadrados_a_marcar = int(self.__cuadrados_a_marcar)

    @staticmethod
    def generar_nonograma_desde_img(img, umbral):
        filas, columnas = img.shape
        nonograma = Nonograma(columnas, filas, tablero=img >= umbral, columnas_no_rellenadas={}, filas_no_rellenadas={})
        nonograma.calcular_cuadros_a_marcar()
        return nonograma

    def agregar(self, tipo: Tipo, idx: int, fila_columna: FilaColumna):
        """
        Permite ir agregado las cabeceras de tipo fila o de tipo columna manualmente
        :param tipo: Si se trata de una fila o de una columna
        :param idx: posición correspondiente
        :param fila_columna: Valor con la información necesaria
        :return: -
        """
        if tipo == Tipo.FILA:
            if fila_columna.get_valor_total() > self.__ancho:
                raise Exception(" Valor de fila superior al soportado")
            self.__filas[idx] = fila_columna
        else:
            if fila_columna.get_valor_total() > self.__alto:
                raise Exception(" Valor de columna superior al soportado")
            self.__columnas[idx] = fila_columna

    def agregar_array(self, tipo: Tipo, idx: int, lista_valores: [int]):
        """
        Permite ir agregado las cabeceras de tipo fila o de tipo columna manualmente
        :param tipo: Si se trata de una fila o de una columna
        :param idx: posición correspondiente
        :param lista_valores: Valor con la información necesaria
        :return: -
        """
        fila_columna = FilaColumna(lista_valores)
        self.agregar(tipo, idx, fila_columna)

    # -------------------------------------------------
    # Generadores
    # -------------------------------------------------

    def rellenar_de_manera_aleatoria(self, umbral=0.5):
        tablero = np.random.rand(self.__alto, self.__ancho) >= umbral
        self.__tablero = Nonograma.__convertir_binario_a_contenido(tablero)
        self.__columnas_no_rellenadas = {}
        self.__filas_no_rellenadas = {}

    def rellenar_de_manera_aleatoria_figuras(self, max_circles=4, max_squares=4, max_lines=4):
        img = np.zeros((self.__alto, self.__ancho, 3), dtype=np.uint8)
        color = [255, 255, 255]
        for i in range(max_circles):
            center = (np.random.randint(0, self.__alto), np.random.randint(0, self.__ancho))
            if self.__alto < self.__ancho:
                radius = np.random.randint(0, self.__alto)
            else:
                radius = np.random.randint(0, self.__ancho)
            img = cv.circle(img, center, radius, color)

        for i in range(max_squares):
            p1 = (np.random.randint(0, self.__alto), np.random.randint(0, self.__ancho))
            p2 = (np.random.randint(0, self.__alto), np.random.randint(0, self.__ancho))
            img = cv.rectangle(img, p1, p2, color)

        for i in range(max_lines):
            p1 = (np.random.randint(0, self.__alto), np.random.randint(0, self.__ancho))
            p2 = (np.random.randint(0, self.__alto), np.random.randint(0, self.__ancho))
            img = cv.line(img, p1, p2, color)
        img = img[:, :, 0]

        tablero = img >= 127
        self.__tablero = Nonograma.__convertir_binario_a_contenido(tablero)
        self.__columnas_no_rellenadas = {}
        self.__filas_no_rellenadas = {}

    def generar_cabeceras(self):
        for i in range(self.__alto):
            self.__filas[i] = FilaColumna.generar_fila_columna(self.__tablero[i, :])
        for j in range(self.__ancho):
            self.__columnas[j] = FilaColumna.generar_fila_columna(self.__tablero[:, j])

    def calcular_cabeceras_actuales(self):
        filas = []
        columnas = []
        for i in range(self.__alto):
            filas.append(FilaColumna.generar_fila_columna(self.__tablero[i, :]))
        for j in range(self.__ancho):
            columnas.append(FilaColumna.generar_fila_columna(self.__tablero[:, j]))
        return filas, columnas

    def calcular_cabeceras_equivocadas(self):
        filas, columnas = self.calcular_cabeceras_actuales()
        dic = {Tipo.FILA: [], Tipo.COLUMNA: [], 'total': 0, 'f_c': [], 'c_c': []}
        for i, (fila_real, fila_p) in enumerate(zip(self.__filas, filas)):
            if fila_real != fila_p:
                dic[Tipo.FILA].append(i)
                dic['total'] = dic['total'] + 1
            # else:
            #     dic['f_c'].append(i)
        for i, (col_real, col_p) in enumerate(zip(self.__columnas, columnas)):
            if col_real != col_p:
                dic[Tipo.COLUMNA].append(i)
                dic['total'] = dic['total'] + 1
            # else:
            #     dic['c_c'].append(i)
        return dic

    def convertir_tablero_con_cabeceras_equivocadas(self):
        dic = self.calcular_cabeceras_equivocadas()
        if dic['total'] != 0:
            for i in range(self.__ancho):
                self.set_fila_columna_no_visitada(Tipo.COLUMNA, i)
            for i in range(self.__alto):
                self.set_fila_columna_no_visitada(Tipo.FILA, i)

        for i in dic[Tipo.FILA]:
            self.__tablero[i, :] = Contenido.EMPTY
            # self.set_fila_columna_no_visitada(Tipo.FILA, i)
        for i in dic[Tipo.COLUMNA]:
            self.__tablero[:, i] = Contenido.EMPTY

        celdas_borradas = np.count_nonzero(self.__tablero == Contenido.EMPTY)
        celdas_rellenadas = (self.__ancho * self.__alto) - celdas_borradas
        #     self.set_fila_columna_no_visitada(Tipo.COLUMNA, i)
        # for i in dic['f_c']:
        #     self.set_fila_columna_visitada(Tipo.FILA, i)
        # for i in dic['c_c']:
        #     self.set_fila_columna_visitada(Tipo.COLUMNA, i)
        dic['celdas_borradas'] = celdas_borradas
        dic['celdas_rellenadas'] = celdas_rellenadas
        return dic

    @staticmethod
    def generar_nonograma_aleatorio(ancho: int = 5, alto: int = 5, umbral: float = 0.5):
        nonograma = Nonograma(ancho, alto)
        nonograma.rellenar_de_manera_aleatoria(umbral=umbral)
        nonograma.generar_cabeceras()
        return nonograma

    @staticmethod
    def generar_nonograma_aleatorio_figuras(ancho: int = 10, alto: int = 10, max_circles=4, max_squares=4, max_lines=4):
        nonograma = Nonograma(ancho, alto)
        nonograma.rellenar_de_manera_aleatoria_figuras(max_circles=max_circles, max_squares=max_squares,
                                                       max_lines=max_lines)
        nonograma.generar_cabeceras()
        nonograma.calcular_cuadros_a_marcar()
        return nonograma

    @staticmethod
    def test():
        nonograma = Nonograma(5, 2)
        tablero = np.array([[True, True, False, True, True], [False, False, False, False, False]])
        nonograma.__tablero = Nonograma.__convertir_binario_a_contenido(tablero)
        nonograma.generar_cabeceras()
        # for fila in nonograma.__filas:
        #     print(fila.comprobar_si_es_trivial(5))
        return nonograma

    # -------------------------------------------------
    # Comprobaciones
    # -------------------------------------------------

    def is_solution(self):
        filas_actuales, columnas_actuales = self.calcular_cabeceras_actuales()
        for i, (fila, fila_actual) in enumerate(zip(self.__filas, filas_actuales)):
            if fila != fila_actual:
                return False
        for i, (columna, columna_actual) in enumerate(zip(self.__columnas, columnas_actuales)):
            if columna != columna_actual:
                return False
        return True

    # -------------------------------------------------
    # Getters
    # -------------------------------------------------

    def get_filas_no_rellenadas(self):
        return list(self.__filas_no_rellenadas.keys())

    def get_columnas_no_rellenadas(self):
        return list(self.__columnas_no_rellenadas.keys())

    def get_numero_filas_columnas_no_rellenadas(self):
        return len(self.__filas_no_rellenadas) + len(self.__columnas_no_rellenadas)

    def get_cabecera_fila_columna(self, tipo: Tipo, idx) -> FilaColumna:
        if tipo == Tipo.FILA:
            return self.__filas[idx]
        elif tipo == Tipo.COLUMNA:
            return self.__columnas[idx]

    def get_ancho_tablero(self):
        return self.__ancho

    def get_alto_tablero(self):
        return self.__alto

    def get_fila_columna_valor(self, tipo: Tipo, idx):
        if tipo == Tipo.FILA:
            return self.__tablero[idx, :]
        elif tipo == Tipo.COLUMNA:
            return self.__tablero[:, idx]

    def get_valor(self, fila_i: int, columna_j: int):
        return self.__tablero[fila_i, columna_j]

    def get_cabeceras_con_padding_y_concatenadas(self):
        filas = []
        columnas = []
        for fila in self.__filas:
            filas.append(fila.get_with_padding(self.__ancho))
        for columna in self.__columnas:
            columnas.append(columna.get_with_padding(self.__alto))
        return np.array(filas + columnas).flatten()

    def get_filas_columnas_con_padding(self, tipo: Tipo, global_reverse=False, local_reverse=False):
        if tipo == Tipo.FILA:
            lista = self.__filas
            valor_p = self.__ancho
        else:
            lista = self.__columnas
            valor_p = self.__alto

        filas_columnas = []
        if global_reverse:
            for v in reversed(lista):
                if local_reverse:
                    filas_columnas.append(v.get_reversed_with_padding(valor_p))
                else:
                    filas_columnas.append(v.get_with_padding(valor_p))
        else:
            for v in lista:
                if local_reverse:
                    filas_columnas.append(v.get_reversed_with_padding(valor_p))
                else:
                    filas_columnas.append(v.get_with_padding(valor_p))
        return filas_columnas

    def get_cabeceras_con_padding_y_concatenadas_invertidas(self):
        return np.array(self.get_filas_columnas_con_padding(Tipo.COLUMNA) +
                        self.get_filas_columnas_con_padding(Tipo.FILA)).flatten()

    def get_cabeceras_con_padding_fci(self):
        return np.array(self.get_filas_columnas_con_padding(Tipo.FILA, local_reverse=True) +
                        self.get_filas_columnas_con_padding(Tipo.COLUMNA, global_reverse=True)).flatten()

    def get_cabeceras_con_padding_fic(self):
        return np.array(self.get_filas_columnas_con_padding(Tipo.FILA, global_reverse=True) +
                        self.get_filas_columnas_con_padding(Tipo.COLUMNA, local_reverse=True)).flatten()

    def get_cabeceras_con_padding_cfi(self):
        return np.array(self.get_filas_columnas_con_padding(Tipo.COLUMNA, local_reverse=True) +
                        self.get_filas_columnas_con_padding(Tipo.FILA, global_reverse=True)).flatten()

    def get_cabeceras_con_padding_cif(self):
        return np.array(self.get_filas_columnas_con_padding(Tipo.COLUMNA, global_reverse=True) +
                        self.get_filas_columnas_con_padding(Tipo.FILA, local_reverse=True)).flatten()

    def get_cabeceras_con_padding_fici(self):
        return np.array(self.get_filas_columnas_con_padding(Tipo.FILA, global_reverse=True, local_reverse=True) +
                        self.get_filas_columnas_con_padding(Tipo.COLUMNA, global_reverse=True, local_reverse=True)
                        ).flatten()

    def get_cabeceras_con_padding_cifi(self):
        return np.array(self.get_filas_columnas_con_padding(Tipo.COLUMNA, global_reverse=True, local_reverse=True) +
                        self.get_filas_columnas_con_padding(Tipo.FILA, global_reverse=True, local_reverse=True)
                        ).flatten()

    def get_8_transformaciones(self):
        vector = [self.get_cabeceras_con_padding_y_concatenadas(),
                  self.get_cabeceras_con_padding_y_concatenadas_invertidas(), self.get_cabeceras_con_padding_fci(),
                  self.get_cabeceras_con_padding_fic(), self.get_cabeceras_con_padding_cfi(),
                  self.get_cabeceras_con_padding_cif(), self.get_cabeceras_con_padding_fici(),
                  self.get_cabeceras_con_padding_cifi()]
        return np.array(vector)

    def get_tablero(self):
        return self.__tablero

    # -----------------------------------------------
    # Setters
    # -----------------------------------------------

    def set_fila_columna_visitada(self, tipo: Tipo, idx):
        if tipo == Tipo.FILA:
            if idx in self.__filas_no_rellenadas:
                self.__filas_no_rellenadas.pop(idx)
        elif tipo == Tipo.COLUMNA:
            if idx in self.__columnas_no_rellenadas:
                self.__columnas_no_rellenadas.pop(idx)

    def set_fila_columna_no_visitada(self, tipo: Tipo, idx):
        if tipo == Tipo.FILA:
            if idx not in self.__filas_no_rellenadas:
                self.__filas_no_rellenadas[idx] = True
        elif tipo == Tipo.COLUMNA:
            if idx not in self.__columnas_no_rellenadas:
                self.__columnas_no_rellenadas[idx] = True

    def set_fila_columna_valor(self, tipo: Tipo, idx, array):
        if tipo == Tipo.FILA:
            self.__tablero[idx, :] = array
        elif tipo == Tipo.COLUMNA:
            self.__tablero[:, idx] = array

    def cambiar_valor(self, fila, columna, valor: Contenido):
        self.__tablero[fila, columna] = valor

    # -------------------------------------------------
    # Utilidades
    # -------------------------------------------------

    def comprobar_si_las_casillas_tienen_valor(self):
        for i in range(self.__alto):
            salida = np.where(self.__tablero[i, :] == Contenido.EMPTY)
            if len(salida[0]) == 0:
                if i in self.__filas_no_rellenadas:
                    self.__filas_no_rellenadas.pop(i)
        for i in range(self.__ancho):
            salida = np.where(self.__tablero[:, i] == Contenido.EMPTY)
            if len(salida[0]) == 0:
                if i in self.__columnas_no_rellenadas:
                    self.__columnas_no_rellenadas.pop(i)

    def copiar(self):
        """
        Método que realiza una copia rápida del tablero
        Las cabeceras se copian en modo referencia mientras que el tablero y las columnas y filas pendientes
        de rellenar se genera una nueva copia
        :return:
        """
        return Nonograma(self.__ancho, self.__alto, self.__filas, self.__columnas, self.__tablero.copy(),
                         self.__columnas_no_rellenadas.copy(), self.__filas_no_rellenadas.copy(),
                         self.__cuadrados_a_marcar)

    def copiar_asignando_nuevo_tablero(self, tablero):
        """
        Método que realiza una copia rápida del tablero
        Las cabeceras se copian en modo referencia mientras que el tablero y las columnas y filas pendientes
        de rellenar se genera una nueva copia
        :return:
        """
        return Nonograma(self.__ancho, self.__alto, self.__filas, self.__columnas, tablero,
                         self.__columnas_no_rellenadas.copy(), self.__filas_no_rellenadas.copy(),
                         self.__cuadrados_a_marcar)

    def copiar_asignando_nuevo_tablero_reset_pendientes(self, tablero):
        """
        Método que realiza una copia rápida del tablero
        Las cabeceras se copian en modo referencia mientras que el tablero se genera una nueva copia
        las columnas y filas pendientes se reinician
        :return:
        """
        return Nonograma(self.__ancho, self.__alto, self.__filas, self.__columnas, tablero,
                         {}, {}, self.__cuadrados_a_marcar)

    @staticmethod
    def __convertir_binario_a_contenido(tablero):
        """
        Convierte un tablero en codificación binaria (celda rellenada o vacía) a un tablero en codificación
        de 3 estados
        :param tablero: tablero binario
        :return: tablero en codificación 3 estados.
        """
        tablero_int = tablero.astype(np.uint8)
        tablero = Nonograma.KEYS[tablero_int]
        return tablero

    # -------------------------------------------------
    # Particle algorithm
    # -------------------------------------------------

    def mutation(self):
        """
        Método que se encarga de realizar una mutación al tablero para el algoritmo de partículas.
        Si el número de casillas coincide, se mueve una casilla rellenada con una casilla vacía
        y en caso contrario, se aumenta o se disminuye a conveniencia.
        :return:
        """
        puntos_where = np.where(self.__tablero == Contenido.FULL)
        puntos = tuple(zip(*puntos_where))
        puntos_marcados = len(puntos)

        puntos_where_empty = np.where(self.__tablero == Contenido.CROSS)
        puntos_empty = tuple(zip(*puntos_where_empty))

        # Si los puntos a marcar cuadran y el tablero no está resuelto
        # Se cambia solamente una posición de un punto marcado a un punto no marcado
        if puntos_marcados == self.__cuadrados_a_marcar and len(puntos_empty) != 0:
            punto_a_cambiar = random.randint(0, puntos_marcados - 1)

            punto_2_a_cambiar = random.randint(0, len(puntos_empty) - 1)

            original = self.__tablero[puntos[punto_a_cambiar][0], puntos[punto_a_cambiar][1]]
            nuevo = self.__tablero[puntos_empty[punto_2_a_cambiar][0], puntos_empty[punto_2_a_cambiar][1]]
            self.__tablero[puntos[punto_a_cambiar][0], puntos[punto_a_cambiar][1]] = nuevo
            self.__tablero[puntos_empty[punto_2_a_cambiar][0], puntos_empty[punto_2_a_cambiar][1]] = original

        # Si la cantidad no coincide, se disminuye o se incrementa a conveniencia
        elif puntos_marcados < self.__cuadrados_a_marcar:
            len_puntos_empty = len(puntos_empty)
            valor_a_activar = random.randint(0, len_puntos_empty - 1)
            self.__tablero[puntos_empty[valor_a_activar][0], puntos_empty[valor_a_activar][1]] = Contenido.FULL
        else:  # elif puntos_marcados > self.__cuadrados_a_marcar:
            valor_a_desactivar = random.randint(0, puntos_marcados - 1)
            self.__tablero[puntos[valor_a_desactivar][0], puntos[valor_a_desactivar][1]] = Contenido.CROSS

    def calcular_peso(self):
        columnas_correctas = self.get_numero_columnas_correctas()
        filas_correctas = self.get_numero_filas_correctas()
        porcentaje_marcado_correcto = self.get_porcentaje_correcto()

        return (columnas_correctas + filas_correctas + (self.__ancho * self.__alto) * porcentaje_marcado_correcto) ** 2

    def calcular_peso2(self):
        peso = 0
        filas_actuales, columnas_actuales = self.calcular_cabeceras_actuales()
        peso += self.__calcular_peso2_parcial(self.__alto, self.__filas, filas_actuales, self.__ancho)
        peso += self.__calcular_peso2_parcial(self.__ancho, self.__columnas, columnas_actuales, self.__alto)
        return peso

    @staticmethod
    def __calcular_peso2_parcial(cantidad_elementos, filas_columnas_reales, filas_columnas_actuales, ancho_alto):
        peso = 0
        for i in range(cantidad_elementos):
            fila_real = filas_columnas_reales[i]
            len_fila_real = fila_real.get_len_lista()
            len_fila_actual = filas_columnas_actuales[i].get_len_lista()
            # Segunda ecuación
            if len_fila_real == 0:
                for valor in filas_columnas_actuales[i].get_valores():
                    peso += valor * ancho_alto
            # primera ecuación
            else:
                # Segunda parte de la ecuación
                peso += abs(len_fila_real - len_fila_actual) * ancho_alto
                # Primera parte de la ecuación
                longitud = np.min(np.array([len_fila_real, len_fila_actual]))
                valores_reales = fila_real.get_valores()
                valores_actuales = filas_columnas_actuales[i].get_valores()
                for j in range(longitud):
                    peso += abs(valores_reales[j] - valores_actuales[j])
        return peso

    def get_porcentaje_correcto(self):
        puntos_where = np.where(self.__tablero == Contenido.FULL)
        puntos = tuple(zip(*puntos_where))
        puntos_marcados = len(puntos)
        diferencia = np.abs(puntos_marcados - self.__cuadrados_a_marcar)
        if puntos_marcados < self.__cuadrados_a_marcar:
            return diferencia / self.__cuadrados_a_marcar
        else:
            return max(0, self.__cuadrados_a_marcar - diferencia / self.__cuadrados_a_marcar)

    def get_numero_columnas_equivocadas(self):
        columnas_equivocadas = 0
        for i, columna in enumerate(self.__columnas):
            if FilaColumna.generar_fila_columna(self.__tablero[:, i]) != columna:
                columnas_equivocadas += 1
        return columnas_equivocadas

    def get_numero_filas_equivocadas(self):
        filas_equivocadas = 0
        for i, fila in enumerate(self.__filas):
            if FilaColumna.generar_fila_columna(self.__tablero[i, :]) != fila:
                filas_equivocadas += 1
        return filas_equivocadas

    def get_numero_columnas_correctas(self):
        return self.__ancho - self.get_numero_columnas_equivocadas()

    def get_numero_filas_correctas(self):
        return self.__alto - self.get_numero_filas_equivocadas()

    # -------------------------------------------------
    # Print
    # -------------------------------------------------

    def __calcular_valores_para_print(self):
        """
        Realiza los cálculos necesarios para definir el espaciado para pintar el tablero
        :return:
        """
        num_elementos_columna = 0
        max_longitud_fila = 0

        # Calcula la altura máxima de las columnas
        for columna in self.__columnas:
            if columna.get_len_lista() > num_elementos_columna:
                num_elementos_columna = columna.get_len_lista()
        # Calcula el número de elementos de una fila y la longitud de la fila
        for fila in self.__filas:
            if fila.get_len_string_equivalent() > max_longitud_fila:
                max_longitud_fila = fila.get_len_string_equivalent()
        return num_elementos_columna, max_longitud_fila

    def __calcular_valores_columna(self, alto_columnas):
        """
        Se convierten las columnas en una notación matricial para
        poder realizar correctamente la representación
        :param alto_columnas:
        :return:
        """
        valores_columna = np.zeros((alto_columnas, self.__ancho), dtype=np.int32)
        for i, columna in enumerate(self.__columnas):
            idx_inicio = alto_columnas - columna.get_len_lista()
            for valor in columna.get_valores():
                valores_columna[idx_inicio, i] = valor
                idx_inicio += 1
        return valores_columna

    def __str__(self) -> str:
        alto_columnas, len_filas = self.__calcular_valores_para_print()
        valores_columna = self.__calcular_valores_columna(alto_columnas)

        string_r = ""

        # Para cada una de las filas correspondientes a las columnas
        for i in range(alto_columnas):
            # Se agrega la tabulación del espacio de las filas
            string_r += " " * len_filas + "|"
            # Para cada una de las columnas
            for j in range(self.__ancho):
                # Si el valor es 0 se dejan n espacios en blanco, dependiendo del ancho de la columna
                if valores_columna[i, j] == 0:
                    string_r += " " * self.__columnas[j].get_ancho_string_equivalent() + "|"
                else:
                    espacios_extra = self.__columnas[j].get_ancho_string_equivalent() - len(str(valores_columna[i, j]))
                    string_r += " " * espacios_extra + str(valores_columna[i, j])
                    string_r += "|"
            string_r += "\n"

        for i, fila in enumerate(self.__filas):
            espacios_extra = len_filas - fila.get_len_string_equivalent()
            string_r += " " * espacios_extra
            # Se pinta la cabecera izquierda
            for j, valor in enumerate(fila.get_valores()):
                if j + 1 < fila.get_len_lista():
                    string_r += str(valor) + " "
                else:
                    string_r += str(valor)
            string_r += "|"
            # Se pinta el tablero
            for j in range(self.__ancho):
                if self.__tablero[i, j] == Contenido.FULL:
                    string_r += "█" * self.__columnas[j].get_ancho_string_equivalent() + "|"
                elif self.__tablero[i, j] == Contenido.EMPTY:
                    string_r += " " * self.__columnas[j].get_ancho_string_equivalent() + "|"
                elif self.__tablero[i, j] == Contenido.CROSS:
                    string_r += "x" * self.__columnas[j].get_ancho_string_equivalent() + "|"
            string_r += "\n"

        return string_r
