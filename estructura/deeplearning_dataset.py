import numpy as np

from estructura import Nonograma


class DeepLearningDataset:

    # ancho, alto
    def __init__(self, size_of_nonogram: (int, int)) -> None:
        self.__size_of_nonogram = size_of_nonogram

    def calcular_todas_las_combinaciones(self, path_train: str = './datos/train/train',
                                         path_target: str = './datos/target/target'):
        combinaciones = []
        cabeceras = []
        vector = np.zeros(self.__size_of_nonogram, dtype=np.uint8)
        contador = 0
        contador2 = 0
        hay_carry = False
        while not hay_carry:
            tablero = Nonograma.KEYS[vector]
            nonograma = Nonograma(ancho=self.__size_of_nonogram[0], alto=self.__size_of_nonogram[1], tablero=tablero)
            vector_f = vector.flatten()
            combinaciones.append(vector_f)
            cabeceras.append(nonograma.get_cabeceras_con_padding_y_concatenadas())

            hay_carry, vector_increased = self.increment_vector(vector_f)
            vector = np.resize(vector_increased, self.__size_of_nonogram)
            contador += 1
            if contador % 200000 == 0:
                contador2 += 1
                contador = 0
                print(contador2)
                np.savez_compressed(path_train+"_"+str(contador2)+".npz", np.array(cabeceras))
                np.savez_compressed(path_target+"_"+str(contador2)+".npz", np.array(combinaciones))
                cabeceras = []
                combinaciones = []

        np.savez_compressed(path_train+"_"+str(contador2+1)+".npz", np.array(cabeceras))
        np.savez_compressed(path_target+"_"+str(contador2+2)+".npz", np.array(combinaciones))

    @staticmethod
    def increment_vector(vector):
        a = vector.copy()
        n = len(a)

        # Add 1 to last digit and find carry
        a[n - 1] += 1
        carry = a[n - 1] // 2
        a[n - 1] = a[n - 1] % 2

        # Traverse from second last digit
        for i in range(n - 2, -1, -1):
            if carry == 1:
                a[i] += 1
                carry = a[i] // 2
                a[i] = a[i] % 2

        if carry == 1:
            return True, a
        else:
            return False, a

    @staticmethod
    def combinar_ficheros(path='./datos/target/', filename='target_', n_files=168,
                          exit_path="./datos/target/target_combined.npz"):

        filenames = []
        for i in range(n_files):
            filenames.append(path+filename+"{}".format(i+1)+".npz")

        combined_data = np.concatenate([np.load(fname)['arr_0'] for fname in filenames])
        np.savez_compressed(exit_path, combined_data)
