import numpy as np
from estructura.nonograma_enums import Contenido


class FilaColumna:

    def __init__(self, lista_valores=[]) -> None:
        self.__lista = np.array(lista_valores)

    @staticmethod
    def generar_fila_columna(array):
        """
        Método encargado de generar la cabecera de una fila o columna concreta
        :param array: fila o columna a la que hay que generar la cabecera
        :return: la cabecera rellenada
        """
        celdas_llenas = 0
        valores = []
        # Para cada una de las celdas
        for valor in array:
            # Si tiene valor se incrementa el contador
            if valor == Contenido.FULL:
                celdas_llenas += 1
            else:
                # En caso contrario se ignoran los espacios en blanco y
                # en caso de que se pase de llenada a vacía se guardan los valores
                if celdas_llenas != 0:
                    valores.append(celdas_llenas)
                    celdas_llenas = 0
        # Si el último valor está rellenado se guarda el resultado
        if celdas_llenas != 0:
            valores.append(celdas_llenas)
        return FilaColumna(valores)

    def comprobar_si_es_trivial(self, fila_columna: int):
        """
        Comprueba si una fila o columna es trivial, esto se puede saber
        si la suma de la cabecera con sus espacios tiene el mismo tamaño
        que la fila o columna que se consulta o si una columna está
        totalmente vacía.
        :param fila_columna:
        :return:
        """
        if self.__lista.sum() + len(self.__lista) - 1 == fila_columna:
            return True
        elif len(self.__lista) == 0:
            return True
        else:
            return False

    def get_valor_total(self):
        """
        Realiza un proceso similar a la función ```comprobar_si_es_trivial```
        pero se retorna el valor concreto
        :return:
        """
        if len(self.__lista) == 0:
            return 0
        valor = self.__lista.sum()
        valor += len(self.__lista) - 1
        return valor

    def get_len_lista(self):
        """
        :return: Retorna la longitud de la cabecera
        """
        return len(self.__lista)

    def get_valores(self):
        return self.__lista

    def get_with_padding(self, ancho_alto: int):
        if ancho_alto % 2 == 0:
            total_len = ancho_alto//2
        else:
            total_len = (ancho_alto+1)//2

        n_padding = total_len - len(self.__lista)
        padding = [0] * n_padding
        lista_nueva = padding + list(self.__lista)
        return lista_nueva

    def get_reversed_with_padding(self, ancho_alto: int):
        total_len = self.get_size_of_fila_columna_with_padding(ancho_alto)
        n_padding = total_len - len(self.__lista)
        padding = [0] * n_padding
        lista_invertida = np.flip(self.__lista)
        lista_nueva = padding + list(lista_invertida)
        return lista_nueva

    @staticmethod
    def get_size_of_fila_columna_with_padding(ancho_alto):
        if ancho_alto % 2 == 0:
            total_len = ancho_alto // 2
        else:
            total_len = (ancho_alto + 1) // 2
        return total_len

    # -------------------------------------------------
    # Utilidades para el __str__ de la clase Nonograma
    # -------------------------------------------------

    def get_len_string_equivalent(self):
        """
        Retorna la longitud que tendría el string correspondiente de una fila.
        Esto es necesario, ya que, hay valores que ocupan más de un carácter.
        :return:
        """
        if len(self.__lista) == 0:
            return 0
        longitud = 0
        for valor in self.__lista:
            longitud += len(str(valor)) + 1  # +1 para el espacio entre valores
        return longitud - 1  # -1 porque el último no tiene espacio final

    def get_ancho_string_equivalent(self):
        """
        Calcula el ancho necesario para representar una columna.
        Se obliga a que sea como mínimo de un elemento
        :return:
        """
        ancho = 1
        for valor in self.__lista:
            if len(str(valor)) > ancho:
                ancho = len(str(valor))
        return ancho

    # -------------------------------------------------
    # Utilidades
    # -------------------------------------------------

    def __eq__(self, o: object) -> bool:
        if isinstance(o, FilaColumna):
            if len(self.__lista) != len(o.__lista):
                return False
        for e1, e2 in zip(self.__lista, o.__lista):
            if e1 != e2:
                return False
        return True

    def __ne__(self, o: object) -> bool:
        return not self.__eq__(o)

    def __str__(self) -> str:
        return str(self.__lista)
