import abc


class TableroInterface(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'obtener_cabeceras_nonograma') and callable(subclass.obtener_cabeceras_nonograma) or
                NotImplemented)

    @abc.abstractmethod
    def obtener_cabeceras_nonograma(self, frame):
        """
        Método encargado de realizar el proceso para obtener la información de la cabecera del tablero
        :return:
        """
        pass
