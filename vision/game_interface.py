import abc


class GameInterface(metaclass=abc.ABCMeta):

    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'es_pantalla_de_juego') and callable(subclass.es_pantalla_de_juego) and
                hasattr(subclass, 'obtener_cabeceras_nonograma') and callable(subclass.obtener_cabeceras_nonograma) and
                hasattr(subclass, 'escalar_imagen') and callable(subclass.escalar_imagen) or
                NotImplemented)

    @abc.abstractmethod
    def es_pantalla_de_juego(self, frame):
        """
        Define si el frame actual se trata de una pantalla donde se encuentra un tablero para jugar o no
        :return:
        """
        pass

    @abc.abstractmethod
    def obtener_cabeceras_nonograma(self, frame):
        """
        Método encargado de realizar el proceso para obtener la información de la cabecera del tablero
        :return:
        """
        pass

    @abc.abstractmethod
    def escalar_imagen(self, frame):
        """
        Escala la imagen
        :return:
        """
        pass
