from vision.picross_toolbox import PicrossS5Toolbox
from vision.tablero_interface import TableroInterface
import cv2
import os


class Picross15x15(TableroInterface):
    PATTERNS_FOLDER = 'vision/patrones/picross_s5/15x15/'

    def __init__(self) -> None:
        super().__init__()

        # Información relevante para la búsqueda de patrones
        self.__n_filas = 15
        self.__n_columnas = 15
        self.__patrones = []
        for files in os.listdir(self.PATTERNS_FOLDER):
            patron = cv2.imread(os.path.join(self.PATTERNS_FOLDER, files), cv2.IMREAD_GRAYSCALE)
            if '_alt' in files:
                self.__patrones.append((int(files[:-8]), patron))
            else:
                self.__patrones.append((int(files[:-4]), patron))
        # Posiciones donde se encuentran las filas en un tablero de 15x15
        # ancho inicio, ancho final, alto
        self.__posiciones_columnas = [(524, 556, 221),  # 1
                                      (556, 588, 221),  # 2
                                      (588, 620, 221),  # 3
                                      (620, 652, 221),  # 4
                                      (652, 685, 221),  # 5
                                      (685, 716, 221),  # 6
                                      (716, 748, 221),  # 7
                                      (748, 780, 221),  # 8
                                      (780, 812, 221),  # 9
                                      (812, 844, 221),  # 10
                                      (844, 876, 221),  # 11
                                      (876, 908, 221),  # 12
                                      (908, 940, 221),  # 13
                                      (940, 972, 221),  # 14
                                      (972, 1004, 221)  # 15
                                      ]
        # ancho inicio, ancho final, alto inicio, alto final
        self.__posiciones_filas = [(270, 524, 221, 253),  # 1
                                   (270, 524, 253, 285),  # 2
                                   (270, 524, 285, 317),  # 3
                                   (270, 524, 317, 349),  # 4
                                   (270, 524, 349, 381),  # 5
                                   (270, 524, 381, 413),  # 6
                                   (270, 524, 413, 445),  # 7
                                   (270, 524, 445, 477),  # 8
                                   (270, 524, 477, 509),  # 9
                                   (270, 524, 509, 540),  # 10
                                   (270, 524, 540, 573),  # 11
                                   (270, 524, 573, 605),  # 12
                                   (270, 524, 605, 637),  # 13
                                   (270, 524, 637, 668),  # 14
                                   (270, 524, 668, 700)   # 15
                                   ]
        self.__picrossTB = PicrossS5Toolbox(self.__n_columnas, self.__n_filas, self.__patrones,
                                            self.__posiciones_columnas, self.__posiciones_filas)

    def obtener_cabeceras_nonograma(self, frame):
        return self.__picrossTB.obtener_cabeceras_nonograma(frame, threshold=0.9, margen_filas=5)
