from vision.picross_toolbox import PicrossS5Toolbox
from vision.tablero_interface import TableroInterface
import cv2
import os


class Picross20x15(TableroInterface):
    PATTERNS_FOLDER = 'vision/patrones/picross_s5/20x15/'

    def __init__(self) -> None:
        super().__init__()

        # Información relevante para la búsqueda de patrones
        self.__n_filas = 15
        self.__n_columnas = 20
        self.__patrones = []
        for files in os.listdir(self.PATTERNS_FOLDER):
            patron = cv2.imread(os.path.join(self.PATTERNS_FOLDER, files), cv2.IMREAD_GRAYSCALE)
            if '_alt' in files:
                self.__patrones.append((int(files[:-8]), patron))
            else:
                self.__patrones.append((int(files[:-4]), patron))
        # Posiciones donde se encuentran las filas en un tablero de 20x15
        # ancho inicio, ancho final, alto
        self.__posiciones_columnas = [(444, 476, 221),  # 1
                                      (476, 508, 221),  # 2
                                      (508, 540, 221),  # 3
                                      (540, 572, 221),  # 4
                                      (572, 604, 221),  # 5
                                      (604, 636, 221),  # 6
                                      (636, 668, 221),  # 7
                                      (668, 700, 221),  # 8
                                      (700, 732, 221),  # 9
                                      (732, 764, 221),  # 10
                                      (764, 796, 221),  # 11
                                      (796, 828, 221),  # 12
                                      (828, 860, 221),  # 13
                                      (860, 892, 221),  # 14
                                      (892, 924, 221),  # 15
                                      (924, 956, 221),  # 16
                                      (956, 988, 221),  # 17
                                      (988, 1020, 221),  # 18
                                      (1020, 1052, 221),  # 19
                                      (1052, 1084, 221),  # 20
                                      ]
        # ancho inicio, ancho final, alto inicio, alto final
        self.__posiciones_filas = [(190, 444, 221, 253),  # 1
                                   (190, 444, 253, 285),  # 2
                                   (190, 444, 285, 317),  # 3
                                   (190, 444, 317, 349),  # 4
                                   (190, 444, 349, 381),  # 5
                                   (190, 444, 381, 413),  # 6
                                   (190, 444, 413, 445),  # 7
                                   (190, 444, 445, 477),  # 8
                                   (190, 444, 477, 509),  # 9
                                   (190, 444, 509, 540),  # 10
                                   (190, 444, 540, 573),  # 11
                                   (190, 444, 573, 605),  # 12
                                   (190, 444, 605, 637),  # 13
                                   (190, 444, 637, 668),  # 14
                                   (190, 444, 668, 700)   # 15
                                   ]
        self.__picrossTB = PicrossS5Toolbox(self.__n_columnas, self.__n_filas, self.__patrones,
                                            self.__posiciones_columnas, self.__posiciones_filas)

    def obtener_cabeceras_nonograma(self, frame):
        return self.__picrossTB.obtener_cabeceras_nonograma(frame, threshold=0.9, margen_filas=5)
