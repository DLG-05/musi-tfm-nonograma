from vision.tablero_interface import TableroInterface
from vision.picross_toolbox import PicrossS5Toolbox
import cv2
import os


class Picross5x5(TableroInterface):

    PATTERNS_FOLDER = 'vision/patrones/picross_s5/5x5/'

    def __init__(self) -> None:
        super().__init__()

        # Información relevante para la búsqueda de patrones
        self.__n_filas = 5
        self.__n_columnas = 5
        self.__patrones = []
        for files in os.listdir(self.PATTERNS_FOLDER):
            patron = cv2.imread(os.path.join(self.PATTERNS_FOLDER, files), cv2.IMREAD_GRAYSCALE)
            self.__patrones.append((int(files[:-4]), patron))
        # Posiciones donde se encuentran las filas en un tablero de 5x5
        # ancho inicio, ancho final, alto
        self.__posiciones_columnas = [(525, 620, 220), (620, 715, 220), (715, 812, 220),
                                      (812, 902, 220), (902, 1004, 220)]
        # ancho inicio, ancho final, alto inicio, alto final
        self.__posiciones_filas = [(270, 525, 220, 317), (270, 525, 317, 412), (270, 525, 412, 510),
                                   (270, 525, 510, 605), (270, 525, 605, 700)]
        self.__picrossTB = PicrossS5Toolbox(self.__n_columnas, self.__n_filas, self.__patrones,
                                            self.__posiciones_columnas, self.__posiciones_filas)

    def obtener_cabeceras_nonograma(self, frame):
        return self.__picrossTB.obtener_cabeceras_nonograma(frame)
