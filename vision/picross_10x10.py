from vision.picross_toolbox import PicrossS5Toolbox
from vision.tablero_interface import TableroInterface
import cv2
import os


class Picross10x10(TableroInterface):
    PATTERNS_FOLDER = 'vision/patrones/picross_s5/10x10/'

    def __init__(self) -> None:
        super().__init__()

        # Información relevante para la búsqueda de patrones
        self.__n_filas = 10
        self.__n_columnas = 10
        self.__patrones = []
        for files in os.listdir(self.PATTERNS_FOLDER):
            patron = cv2.imread(os.path.join(self.PATTERNS_FOLDER, files), cv2.IMREAD_GRAYSCALE)
            if '_alt' in files:
                self.__patrones.append((int(files[:-8]), patron))
            else:
                self.__patrones.append((int(files[:-4]), patron))
        # Posiciones donde se encuentran las filas en un tablero de 10x10
        # ancho inicio, ancho final, alto
        self.__posiciones_columnas = [(524, 572, 221),  # 1
                                      (572, 620, 221),  # 2
                                      (620, 668, 221),  # 3
                                      (668, 716, 221),  # 4
                                      (716, 764, 221),  # 5
                                      (764, 812, 221),  # 6
                                      (812, 860, 221),  # 7
                                      (860, 908, 221),  # 8
                                      (908, 956, 221),  # 9
                                      (956, 1004, 221)  # 10
                                      ]
        # ancho inicio, ancho final, alto inicio, alto final
        self.__posiciones_filas = [(270, 524, 221, 269),  # 1
                                   (270, 524, 269, 317),  # 2
                                   (270, 524, 317, 365),  # 3
                                   (270, 524, 365, 413),  # 4
                                   (270, 524, 413, 461),  # 5
                                   (270, 524, 461, 509),  # 6
                                   (270, 524, 509, 557),  # 7
                                   (270, 524, 557, 605),  # 8
                                   (270, 524, 605, 652),  # 9
                                   (270, 524, 652, 700)   # 10
                                   ]
        self.__picrossTB = PicrossS5Toolbox(self.__n_columnas, self.__n_filas, self.__patrones,
                                            self.__posiciones_columnas, self.__posiciones_filas)

    def obtener_cabeceras_nonograma(self, frame):
        return self.__picrossTB.obtener_cabeceras_nonograma(frame, margen_filas=5)
