from enum import IntEnum

import cv2

from estructura import Nonograma, Tipo
from vision.game_interface import GameInterface
from vision.picross_5x5 import Picross5x5
from vision.picross_10x10 import Picross10x10
from vision.picross_15x15 import Picross15x15
from vision.picross_20x15 import Picross20x15
from vision.tablero_interface import TableroInterface


class ModoDetection(IntEnum):
    AUTO = 0
    M_5x5 = 1
    M_10x10 = 2
    M_15x15 = 3
    M_20x15 = 4


class PicrossS5(GameInterface):
    DEBUG = True

    PATH_CABECERA = "./vision/patrones/picross_s5/common/cabecera.png"
    WAIT_TIME = 24
    THRESHOLD_ES_PANTALLA_J = 0.8

    def __init__(self, modo: ModoDetection) -> None:
        super().__init__()
        # Información relevante para detectar si es pantalla de juego
        self.__cabecera = cv2.imread(self.PATH_CABECERA, cv2.IMREAD_COLOR)
        self.__cabecera_h, self.__cabecera_w = self.__cabecera.shape[0:2]
        self.__contador = 0
        # Información del modo
        self.__modo = modo
        # Detectores posibles
        self.__detectores = {ModoDetection.M_5x5: Picross5x5(), ModoDetection.M_10x10: Picross10x10(),
                             ModoDetection.M_15x15: Picross15x15(), ModoDetection.M_20x15: Picross20x15()}
        # Representado en ancho / alto
        self.__filas_columnas_modo = {ModoDetection.M_5x5: (5, 5), ModoDetection.M_10x10: (10, 10),
                                      ModoDetection.M_15x15: (15, 15), ModoDetection.M_20x15: (20, 15)}

    # -------------------------------------------------
    # Métodos de la interfaz
    # -------------------------------------------------

    def es_pantalla_de_juego(self, frame):
        """
        Método que comprueba si el frame que se pasa es un frame de la pantalla de juego o no
        :param frame: Frame actual
        :return:
        """
        frame_c = None
        if self.DEBUG:
            frame_c = frame.copy()
        # Se mira si se detecta la cabecera del juego
        res = cv2.matchTemplate(frame, self.__cabecera, cv2.TM_CCOEFF_NORMED)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
        if max_val >= self.THRESHOLD_ES_PANTALLA_J:
            top_left = max_loc
            # Si es así se pinta y se espera para decir que está en partida
            # Se realiza para evitar el mensaje de adelante
            if self.DEBUG:
                cv2.rectangle(frame_c, top_left, (top_left[0] + self.__cabecera_w, top_left[1] + self.__cabecera_h),
                              (0, 0, 255), 2)
            if self.__contador < self.WAIT_TIME:
                self.__contador += 1
                cabecera_detectada = False
            else:
                cabecera_detectada = True
        # Si no se detecta, se reinicia el contador y se indica que no está en partida
        else:
            self.__contador = 0
            cabecera_detectada = False
        if self.DEBUG:
            return frame_c, cabecera_detectada
        return frame, cabecera_detectada

    def obtener_cabeceras_nonograma(self, frame):
        """
        Método que permite obtener las cabeceras de un tablero nonograma
        :param frame: Frame actual
        :return: Nonograma
        """
        detector, modo = self.__determinar_detector(frame)
        frame_conv, columnas, filas = detector.obtener_cabeceras_nonograma(frame)
        if self.DEBUG:
            cv2.imshow('frame2', frame_conv)
        return self.__convertir_a_nonograma(filas, columnas, modo), frame_conv

    def escalar_imagen(self, frame):
        """
        Método que escala la imagen si no está a 1280x720
        :param frame:
        :return:
        """
        h, w = frame.shape[0:2]
        if w != 1280 or h != 720:
            return cv2.resize(frame, (1280, 720))
        return frame

    # -------------------------------------------------
    # Determinación de detectores
    # -------------------------------------------------

    def __determinar_detector(self, frame) -> (TableroInterface, ModoDetection):
        """
        Método encargado de determinar el detector necesario para el tablero
        :param frame: frame del tablero
        :return: (Detector, Modo)
        """
        if self.__modo == ModoDetection.AUTO:
            return self.__determinar_auto(frame)
        else:
            return self.__detectores[self.__modo], self.__modo

    def __determinar_auto(self, frame) -> (TableroInterface, ModoDetection):
        """
        Método que determina automáticamente el modo en que debe trabajar
        :param frame: frame del tablero
        :return: (Detector, Modo)
        """
        frame_g = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        ret3, img_bin = cv2.threshold(frame_g, 210, 255, cv2.THRESH_BINARY)

        # cv2.imwrite('frame_2.png', img_bin)
        # cv2.imshow('frame3', img_bin)

        # mirando primero los tableros de mayor dimension a menor
        if self.__comprobar_si_es_20x15(img_bin):
            print(ModoDetection.M_20x15)
            return self.__detectores[ModoDetection.M_20x15], ModoDetection.M_20x15
        elif self.__comprobar_si_es_15x15(img_bin):
            print(ModoDetection.M_15x15)
            return self.__detectores[ModoDetection.M_15x15], ModoDetection.M_15x15
        elif self.__comprobar_si_es_10x10(img_bin):
            print(ModoDetection.M_10x10)
            return self.__detectores[ModoDetection.M_10x10], ModoDetection.M_10x10
        elif self.__comprobar_si_es_5x5(img_bin):
            print(ModoDetection.M_5x5)
            return self.__detectores[ModoDetection.M_5x5], ModoDetection.M_5x5

        return self.__detectores[ModoDetection.M_5x5], ModoDetection.M_5x5

    @staticmethod
    def __comprobar_si_es_5x5(img):
        """
        Comprueba si un tablero es de 5x5 mirando los círculos en la parte inferior
        :param img: frame
        :return: si es o no es un tablero 5x5
        """
        puntos = [(620, 605), (716, 605), (812, 605), (909, 605)]
        for (ancho, alto) in puntos:
            if img[alto, ancho] != 0:
                return False
        return True

    @staticmethod
    def __comprobar_si_es_10x10(img):
        """
        Comprueba si un tablero es de 10x10 mirando los círculos en la parte inferior
        :param img: frame
        :return: si es o no es un tablero 10x10
        """

        puntos = [(572, 653), (620, 653), (668, 653), (716, 653), (764, 653), (812, 653), (860, 653), (908, 653),
                  (956, 653)]
        for (ancho, alto) in puntos:
            if img[alto, ancho] != 0:
                return False
        return True

    @staticmethod
    def __comprobar_si_es_15x15(img):
        """
        Comprueba si un tablero es de 15x15 mirando los círculos en la parte inferior
        :param img: frame
        :return: si es o no es un tablero 15x15
        """
        puntos = [(556, 668), (588, 668), (620, 668), (652, 668), (685, 668), (716, 668), (748, 668), (780, 668),
                  (812, 668), (844, 668), (876, 668), (908, 668), (940, 668)]
        for (ancho, alto) in puntos:
            if img[alto, ancho] != 0:
                return False
        return True

    @staticmethod
    def __comprobar_si_es_20x15(img):
        """
        Comprueba si un tablero es de 20x15 mirando los círculos en la parte inferior
        :param img: frame
        :return: si es o no es un tablero 20x15
        """
        puntos = [(476, 668), (508, 668), (540, 668), (572, 668), (604, 668), (636, 668), (668, 668), (700, 668),
                  (732, 668), (764, 668), (796, 668), (828, 668), (860, 668), (892, 668), (924, 668), (956, 668),
                  (988, 668), (1020, 668)]
        for (ancho, alto) in puntos:
            if img[alto, ancho] != 0:
                return False
        return True

    # -------------------------------------------------
    # Transformador a Nonograma
    # -------------------------------------------------

    def __convertir_a_nonograma(self, filas, columnas, modo):
        """
        Método que convierte las filas y columnas filtradas a una estructura Nonograma
        :param filas: filas filtradas
        :param columnas: columnas filtradas
        :param modo: Modo de trabajo
        :return: Nonograma
        """
        ancho, alto = self.__filas_columnas_modo[modo]
        nonograma = Nonograma(ancho=ancho, alto=alto)
        for idx, fila in enumerate(filas):
            nonograma.agregar_array(Tipo.FILA, idx, fila)
        for idx, columna in enumerate(columnas):
            nonograma.agregar_array(Tipo.COLUMNA, idx, columna)
        nonograma.calcular_cuadros_a_marcar()
        return nonograma
