import cv2
import numpy as np


class PicrossS5Toolbox:

    def __init__(self, n_columnas, n_filas, patrones, posiciones_columnas, posiciones_filas) -> None:
        super().__init__()
        self.__n_columnas = n_columnas
        self.__n_filas = n_filas
        self.__patrones = patrones
        self.__posiciones_columnas = posiciones_columnas
        self.__posiciones_filas = posiciones_filas

    def __obtener_patrones(self, img_bin, threshold, draw=True):
        columnas = []
        filas = []
        for i in range(self.__n_columnas):
            columnas.append([])
        for i in range(self.__n_filas):
            filas.append([])
        img_draw = None
        if draw:
            img_draw = img_bin.copy()
        # Se buscan todos los patrones
        for (n_numero, img_numero) in self.__patrones:
            res = cv2.matchTemplate(img_bin, img_numero, cv2.TM_CCOEFF_NORMED)
            loc = np.where(res > threshold)
            h, w = img_numero.shape
            for pt in zip(*loc[::-1]):
                # Asignación de los patrones a las posiciones de la fila
                saltar_filas = False
                for i, columna in enumerate(self.__posiciones_columnas):
                    # altura - ancho
                    if (0 <= pt[1] < columna[2]) and (columna[0] <= pt[0] < columna[1]):
                        columnas[i].append((n_numero, pt))
                        saltar_filas = True
                        break
                if not saltar_filas:
                    for i, fila in enumerate(self.__posiciones_filas):
                        # ancho inicio, ancho final, alto inicio, alto final
                        # Comprobación del alto y del ancho
                        if (fila[2] <= pt[1] < fila[3]) and (fila[0] <= pt[0] < fila[1]):
                            filas[i].append((n_numero, pt))
                            break
                if draw:
                    cv2.rectangle(img_draw, pt, (pt[0] + w, pt[1] + h), (0, 0, 255), 2)
        if draw:
            return columnas, filas, img_draw
        else:
            return columnas, filas, img_bin

    @staticmethod
    def __get_altura_ancho(valor, modo):
        if modo == 'columna':
            return valor[1][1]
        elif modo == 'fila':
            return valor[1][0]
        else:
            return None

    def __filtrar_patrones(self, filas_columnas, modo, margen=20):
        filas_columnas_filtradas = []
        filas_columnas_ordenadas = []
        if modo == 'fila':
            valor = self.__n_filas
            # Ordenar las filas y columnas por su dimensión predominante
            for fila_columna in filas_columnas:
                filas_columnas_ordenadas.append(sorted(fila_columna, key=lambda x: x[1][0], reverse=False))
        elif modo == 'columna':
            valor = self.__n_columnas
            for fila_columna in filas_columnas:
                filas_columnas_ordenadas.append(sorted(fila_columna, key=lambda x: x[1][1], reverse=False))
        else:
            raise Exception("Modo incorrecto en __filtrar_patrones")
        for i in range(valor):
            filas_columnas_filtradas.append([])

        valor_previo = (-1, (-1, -1))
        # Para cada uno de los patrones detectados en cada una de las filas o columnas
        for i, fila_columna in enumerate(filas_columnas_ordenadas):
            for patron in fila_columna:
                diferencia = 0
                if modo == 'columna':
                    diferencia = np.abs(valor_previo[1][1] - patron[1][1])
                elif modo == 'fila':
                    diferencia = np.abs(valor_previo[1][0] - patron[1][0])
                # Si el valor que viene a continuación es diferente se agrega el antiguo
                if valor_previo[0] != patron[0] and valor_previo[0] != -1:
                    altura_ancho = self.__get_altura_ancho(valor_previo, modo)
                    filas_columnas_filtradas[i].append((valor_previo[0], altura_ancho))
                # Si la diferencia es mayor a un umbral se agrega
                elif valor_previo[0] != -1 and diferencia > margen:
                    altura_ancho = self.__get_altura_ancho(valor_previo, modo)
                    filas_columnas_filtradas[i].append((valor_previo[0], altura_ancho))
                # Se actualiza el valor previo
                valor_previo = patron
            # Si se está situado en el último elemento
            if valor_previo[0] != -1:
                altura_ancho = self.__get_altura_ancho(valor_previo, modo)
                filas_columnas_filtradas[i].append((valor_previo[0], altura_ancho))
            # Se reinicia el valor previo
            valor_previo = (-1, (-1, -1))

        # Se ordenan los valores filtrados según su posición en el tablero
        filas_columnas_ordenadas = []
        for fila_columna in filas_columnas_filtradas:
            fila_columna_s = sorted(fila_columna, key=lambda x: x[1], reverse=False)
            filas_columnas_ordenadas.append([x[0] for x in fila_columna_s])
        return filas_columnas_ordenadas

    def obtener_cabeceras_nonograma(self, frame, threshold=0.9, margen_filas=20, margen_columnas=20):
        # Aplicar umbral a la imagen para convertirla en binaria
        frame_g = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        ret3, img_bin = cv2.threshold(frame_g, 127, 255, cv2.THRESH_BINARY)

        columnas, filas, img_draw = self.__obtener_patrones(img_bin, threshold=threshold)

        columnas_filtradas = self.__filtrar_patrones(columnas, 'columna', margen=margen_columnas)
        filas_filtradas = self.__filtrar_patrones(filas, 'fila', margen=margen_filas)

        return img_draw, columnas_filtradas, filas_filtradas
