from algoritmos.backtracking_voraz import BacktrackingVoraz
from algoritmos.backtracking_voraz_with_cells import BacktrackingVorazConCeldas
from algoritmos.particles_algorithm import ParticleAlgorithm
from estructura import Nonograma, Tipo, FilaColumna
import time


def modelo_1():
    m1 = Nonograma(2, 2)
    m1.agregar(Tipo.FILA, 0, FilaColumna([1]))
    m1.agregar(Tipo.FILA, 1, FilaColumna([2]))
    m1.agregar(Tipo.COLUMNA, 0, FilaColumna([2]))
    m1.agregar(Tipo.COLUMNA, 1, FilaColumna([1]))
    m1.calcular_cuadros_a_marcar()
    return m1


def modelo_2():
    m2 = Nonograma(5, 5)
    m2.agregar(Tipo.FILA, 0, FilaColumna([1, 1]))
    m2.agregar(Tipo.FILA, 1, FilaColumna([1, 1]))
    m2.agregar(Tipo.FILA, 2, FilaColumna([1, 1]))
    m2.agregar(Tipo.FILA, 3, FilaColumna([5]))
    m2.agregar(Tipo.FILA, 4, FilaColumna([2]))
    m2.agregar(Tipo.COLUMNA, 0, FilaColumna([4]))
    m2.agregar(Tipo.COLUMNA, 1, FilaColumna([1]))
    m2.agregar(Tipo.COLUMNA, 2, FilaColumna([1]))
    m2.agregar(Tipo.COLUMNA, 3, FilaColumna([2]))
    m2.agregar(Tipo.COLUMNA, 4, FilaColumna([5]))
    m2.calcular_cuadros_a_marcar()
    return m2


def modelo_3():
    m3 = Nonograma(5, 5)
    m3.agregar(Tipo.FILA, 0, FilaColumna([3]))
    m3.agregar(Tipo.FILA, 1, FilaColumna([1, 2]))
    m3.agregar(Tipo.FILA, 2, FilaColumna([5]))
    m3.agregar(Tipo.FILA, 3, FilaColumna([3]))
    m3.agregar(Tipo.FILA, 4, FilaColumna([2]))
    m3.agregar(Tipo.COLUMNA, 0, FilaColumna([2]))
    m3.agregar(Tipo.COLUMNA, 1, FilaColumna([1, 2]))
    m3.agregar(Tipo.COLUMNA, 2, FilaColumna([1, 3]))
    m3.agregar(Tipo.COLUMNA, 3, FilaColumna([5]))
    m3.agregar(Tipo.COLUMNA, 4, FilaColumna([2]))
    m3.calcular_cuadros_a_marcar()
    return m3


def modelo_4():
    m4 = Nonograma(10, 10)
    m4.agregar(Tipo.FILA, 0, FilaColumna([2, 2]))
    m4.agregar(Tipo.FILA, 1, FilaColumna([1, 8]))
    m4.agregar(Tipo.FILA, 2, FilaColumna([5, 2]))
    m4.agregar(Tipo.FILA, 3, FilaColumna([4, 3]))
    m4.agregar(Tipo.FILA, 4, FilaColumna([1, 4, 1]))
    m4.agregar(Tipo.FILA, 5, FilaColumna([8]))
    m4.agregar(Tipo.FILA, 6, FilaColumna([3, 2]))
    m4.agregar(Tipo.FILA, 7, FilaColumna([4, 1]))
    m4.agregar(Tipo.FILA, 8, FilaColumna([8]))
    m4.agregar(Tipo.FILA, 9, FilaColumna([4, 3]))
    m4.agregar(Tipo.COLUMNA, 0, FilaColumna([3, 1]))
    m4.agregar(Tipo.COLUMNA, 1, FilaColumna([1, 5, 2]))
    m4.agregar(Tipo.COLUMNA, 2, FilaColumna([3, 5]))
    m4.agregar(Tipo.COLUMNA, 3, FilaColumna([9]))
    m4.agregar(Tipo.COLUMNA, 4, FilaColumna([5, 2]))
    m4.agregar(Tipo.COLUMNA, 5, FilaColumna([1, 2, 2]))
    m4.agregar(Tipo.COLUMNA, 6, FilaColumna([1, 3, 1]))
    m4.agregar(Tipo.COLUMNA, 7, FilaColumna([1, 1, 5]))
    m4.agregar(Tipo.COLUMNA, 8, FilaColumna([7, 2]))
    m4.agregar(Tipo.COLUMNA, 9, FilaColumna([3, 1]))
    m4.calcular_cuadros_a_marcar()
    return m4


def modelo_5():
    m5 = Nonograma(15, 15)
    m5.agregar(Tipo.FILA, 0, FilaColumna([4, 3]))
    m5.agregar(Tipo.FILA, 1, FilaColumna([6, 3]))
    m5.agregar(Tipo.FILA, 2, FilaColumna([1, 4, 3]))
    m5.agregar(Tipo.FILA, 3, FilaColumna([2, 4, 5]))
    m5.agregar(Tipo.FILA, 4, FilaColumna([2, 4, 3, 1]))
    m5.agregar(Tipo.FILA, 5, FilaColumna([6, 4, 2]))
    m5.agregar(Tipo.FILA, 6, FilaColumna([12]))
    m5.agregar(Tipo.FILA, 7, FilaColumna([8, 4, 1]))
    m5.agregar(Tipo.FILA, 8, FilaColumna([6, 6]))
    m5.agregar(Tipo.FILA, 9, FilaColumna([4, 5]))
    m5.agregar(Tipo.FILA, 10, FilaColumna([1, 4, 5]))
    m5.agregar(Tipo.FILA, 11, FilaColumna([2, 4, 3]))
    m5.agregar(Tipo.FILA, 12, FilaColumna([6, 3]))
    m5.agregar(Tipo.FILA, 13, FilaColumna([5, 3]))
    m5.agregar(Tipo.FILA, 14, FilaColumna([4, 3]))
    m5.agregar(Tipo.COLUMNA, 0, FilaColumna([3, 1, 2]))
    m5.agregar(Tipo.COLUMNA, 1, FilaColumna([3, 1, 2]))
    m5.agregar(Tipo.COLUMNA, 2, FilaColumna([1, 4, 2]))
    m5.agregar(Tipo.COLUMNA, 3, FilaColumna([15]))
    m5.agregar(Tipo.COLUMNA, 4, FilaColumna([15]))
    m5.agregar(Tipo.COLUMNA, 5, FilaColumna([15]))
    m5.agregar(Tipo.COLUMNA, 6, FilaColumna([15]))
    m5.agregar(Tipo.COLUMNA, 7, FilaColumna([1, 3]))
    m5.agregar(Tipo.COLUMNA, 8, FilaColumna([1, 2, 1]))
    m5.agregar(Tipo.COLUMNA, 9, FilaColumna([15]))
    m5.agregar(Tipo.COLUMNA, 10, FilaColumna([15]))
    m5.agregar(Tipo.COLUMNA, 11, FilaColumna([15]))
    m5.agregar(Tipo.COLUMNA, 12, FilaColumna([1, 5]))
    m5.agregar(Tipo.COLUMNA, 13, FilaColumna([2, 2]))
    m5.agregar(Tipo.COLUMNA, 14, FilaColumna([2, 2]))
    m5.calcular_cuadros_a_marcar()
    return m5


def modelo_6():
    m6 = Nonograma(20, 5)
    m6.agregar(Tipo.FILA, 0, FilaColumna([1, 1]))
    m6.agregar(Tipo.FILA, 1, FilaColumna([2, 2]))
    m6.agregar(Tipo.FILA, 2, FilaColumna([5, 3]))
    m6.agregar(Tipo.FILA, 3, FilaColumna([5, 3]))
    m6.agregar(Tipo.FILA, 4, FilaColumna([20]))
    m6.agregar(Tipo.COLUMNA, 0, FilaColumna([1, 1]))
    m6.agregar(Tipo.COLUMNA, 1, FilaColumna([1, 1]))
    m6.agregar(Tipo.COLUMNA, 2, FilaColumna([1]))
    m6.agregar(Tipo.COLUMNA, 3, FilaColumna([1]))
    m6.agregar(Tipo.COLUMNA, 4, FilaColumna([1]))
    m6.agregar(Tipo.COLUMNA, 5, FilaColumna([3]))
    m6.agregar(Tipo.COLUMNA, 6, FilaColumna([3]))
    m6.agregar(Tipo.COLUMNA, 7, FilaColumna([3]))
    m6.agregar(Tipo.COLUMNA, 8, FilaColumna([3]))
    m6.agregar(Tipo.COLUMNA, 9, FilaColumna([1, 3]))
    m6.agregar(Tipo.COLUMNA, 10, FilaColumna([1]))
    m6.agregar(Tipo.COLUMNA, 11, FilaColumna([1, 1]))
    m6.agregar(Tipo.COLUMNA, 12, FilaColumna([1]))
    m6.agregar(Tipo.COLUMNA, 13, FilaColumna([3]))
    m6.agregar(Tipo.COLUMNA, 14, FilaColumna([3]))
    m6.agregar(Tipo.COLUMNA, 15, FilaColumna([3]))
    m6.agregar(Tipo.COLUMNA, 16, FilaColumna([1]))
    m6.agregar(Tipo.COLUMNA, 17, FilaColumna([1]))
    m6.agregar(Tipo.COLUMNA, 18, FilaColumna([1, 1]))
    m6.agregar(Tipo.COLUMNA, 19, FilaColumna([1, 1]))
    m6.calcular_cuadros_a_marcar()
    return m6


def modelo_7():
    # Tiempo: 1211.016 -> 20.2 min
    m7 = Nonograma(15, 15)
    m7.agregar(Tipo.FILA, 0, FilaColumna([1, 2]))
    m7.agregar(Tipo.FILA, 1, FilaColumna([2, 2, 1, 1]))
    m7.agregar(Tipo.FILA, 2, FilaColumna([1, 4, 2, 3]))
    m7.agregar(Tipo.FILA, 3, FilaColumna([1, 1, 5, 1, 1]))
    m7.agregar(Tipo.FILA, 4, FilaColumna([2, 1, 6]))
    m7.agregar(Tipo.FILA, 5, FilaColumna([10]))
    m7.agregar(Tipo.FILA, 6, FilaColumna([7, 5]))
    m7.agregar(Tipo.FILA, 7, FilaColumna([4, 1, 4]))
    m7.agregar(Tipo.FILA, 8, FilaColumna([7, 6]))
    m7.agregar(Tipo.FILA, 9, FilaColumna([4, 2, 1]))
    m7.agregar(Tipo.FILA, 10, FilaColumna([2, 1, 3, 1]))
    m7.agregar(Tipo.FILA, 11, FilaColumna([5, 3]))
    m7.agregar(Tipo.FILA, 12, FilaColumna([3, 3, 3, 1]))
    m7.agregar(Tipo.FILA, 13, FilaColumna([1, 1, 3, 4]))
    m7.agregar(Tipo.FILA, 14, FilaColumna([4, 1, 1]))
    m7.agregar(Tipo.COLUMNA, 0, FilaColumna([2, 3, 1]))
    m7.agregar(Tipo.COLUMNA, 1, FilaColumna([4, 5, 2]))
    m7.agregar(Tipo.COLUMNA, 2, FilaColumna([1, 6, 1]))
    m7.agregar(Tipo.COLUMNA, 3, FilaColumna([3, 5]))
    m7.agregar(Tipo.COLUMNA, 4, FilaColumna([8, 1, 1]))
    m7.agregar(Tipo.COLUMNA, 5, FilaColumna([1, 2, 1, 5]))
    m7.agregar(Tipo.COLUMNA, 6, FilaColumna([2, 2, 1, 2, 1]))
    m7.agregar(Tipo.COLUMNA, 7, FilaColumna([4, 1, 2]))
    m7.agregar(Tipo.COLUMNA, 8, FilaColumna([4, 1, 1, 1]))
    m7.agregar(Tipo.COLUMNA, 9, FilaColumna([5, 8]))
    m7.agregar(Tipo.COLUMNA, 10, FilaColumna([4, 1, 1, 1]))
    m7.agregar(Tipo.COLUMNA, 11, FilaColumna([5, 1, 2]))
    m7.agregar(Tipo.COLUMNA, 12, FilaColumna([4, 3, 1, 1]))
    m7.agregar(Tipo.COLUMNA, 13, FilaColumna([1, 3, 5]))
    m7.agregar(Tipo.COLUMNA, 14, FilaColumna([2, 4, 1, 1]))
    m7.calcular_cuadros_a_marcar()
    return m7


def modelo_8():
    m8 = Nonograma(2, 2)
    m8.agregar(Tipo.FILA, 0, FilaColumna([]))
    m8.agregar(Tipo.FILA, 1, FilaColumna([]))
    m8.agregar(Tipo.COLUMNA, 0, FilaColumna([]))
    m8.agregar(Tipo.COLUMNA, 1, FilaColumna([]))
    m8.calcular_cuadros_a_marcar()
    return m8


modelos = [modelo_1(), modelo_2(), modelo_3(), modelo_4(), modelo_5(), modelo_6(), modelo_8(), modelo_7()]
# modelos = [modelo_2(), modelo_3()]
for i, nonograma in enumerate(modelos):
    print(nonograma)
    algoritmo = BacktrackingVorazConCeldas(nonograma)
    tiempo_inicio = time.monotonic()
    algoritmo.run()
    tiempo = time.monotonic() - tiempo_inicio
    print(round(tiempo, 4))
