from os.path import join

from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras.models import load_model, Model
from tqdm.keras import TqdmCallback
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


def fit_model_10x10(model, x_train, y_train, x_test, y_test, num_model: int, epochs=200, patience=10,
                    tqdm_enabled=1, verbose=0, batch_size=100,
                    path_models="./modelos/", name_models="modelo10x10_"):
    callbacks = []

    if patience is not None:
        callbacks.append(EarlyStopping(monitor='val_loss', patience=patience))

    if tqdm_enabled is not None:
        verbose = 0
        callbacks.append(TqdmCallback(verbose=tqdm_enabled))

    callbacks.append(ModelCheckpoint(filepath=path_models + name_models + str(num_model) + ".h5", save_best_only=True,
                                     monitor='val_loss'))

    steps_per_epoch = len(x_train) // batch_size

    history = model.fit(x=x_train, y=y_train, epochs=epochs, validation_data=(x_test, y_test), verbose=verbose,
                        callbacks=callbacks, batch_size=batch_size, steps_per_epoch=steps_per_epoch)

    val_loss_np = np.array(history.history['val_loss'])

    val_loss = np.min(val_loss_np)
    val_accuracy = history.history['val_accuracy'][np.argmin(val_loss_np)]

    fichero_resultados = open(path_models + "results10x10.txt", 'a')
    fichero_resultados.write("modelo 10x10 {},{},{}\n".format(num_model, val_loss, val_accuracy))
    fichero_resultados.close()

    return history, val_loss, val_accuracy


def predecir_dataset_de_val(modelo, x_test, y_test, batch_size):
    predicciones = modelo.predict(x_test, batch_size=batch_size)
    predicciones_adaptadas = predicciones.round().astype(np.uint8)

    contador = 0
    n_errores = np.zeros(100)
    for prediction, real in zip(predicciones_adaptadas, y_test):
        valor = np.all(prediction == real)
        if valor:
            contador += 1

        n_err = len(np.where(prediction != real)[0])
        n_errores[n_err] = n_errores[n_err] + 1

    return (contador / len(y_test)) * 100, pd.DataFrame({'n_errores': np.linspace(0, 99, 100), 'n_tableros': n_errores})


def cargar_modelo(num_model: int, path_models="./modelos/",
                  name_models="modelo10x10_") -> Model:
    return load_model(path_models + name_models + str(num_model) + ".h5")


def fit_and_metrics(modelo, n_model, x_train, y_train, x_test, y_test, epochs=200, patience=10, batch_size=100,
                    colab=False):
    if colab:
        path_models = '/content/drive/MyDrive/Colab Notebooks/TFM/modelos_10x10/'
    else:
        path_models = "./modelos/"
    _, val_loss, val_accuracy = fit_model_10x10(modelo, x_train, y_train, x_test, y_test,
                                                n_model, epochs=epochs, patience=patience, batch_size=batch_size,
                                                path_models=path_models)
    print("Modelo {} - val_loss: {} - val_accuracy: {}".format(n_model, val_loss, val_accuracy))

    val_mil, df_error = predecir_dataset_de_val(cargar_modelo(n_model, path_models=path_models), x_test, y_test,
                                                batch_size=len(y_test))
    print(
        "Modelo {} - % de los tableros de validación están completamente bien predichos: {}".format(n_model, val_mil))
    plt.figure()
    plt.plot(df_error['n_tableros'])
    plt.show()

    df_error.to_csv(join(path_models, "modelo10x10_{}.csv".format(n_model)))

    return df_error
