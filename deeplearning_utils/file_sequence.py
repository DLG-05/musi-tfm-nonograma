import tensorflow as tf
import os
import numpy as np


class FileGenerator(tf.keras.utils.Sequence):
    """
    Secuencia de Keras para poder cargar las combinaciones de un tablero 5x5 y poder pasar
    el contenido preparado para que la red neuronal lo pueda leer
    """
    def __init__(self, ruta) -> None:
        self.train_path = os.path.join(ruta, "train")
        self.target_path = os.path.join(ruta, "target")

        self.train_files = sorted(os.listdir(self.train_path), key=lambda x: int(''.join(filter(str.isdigit, x))))
        self.target_files = sorted(os.listdir(self.target_path), key=lambda x: int(''.join(filter(str.isdigit, x))))

        self.files = list(zip(self.train_files, self.target_files))

        train_file = self.train_files[0]
        self.batch_size = np.load(os.path.join(self.train_path, train_file))['arr_0'].shape[0]

    def __len__(self):
        return len(self.files)

    def __getitem__(self, index):
        train_file, target_file = self.files[index]
        train5_5 = np.load(os.path.join(self.train_path, train_file))['arr_0']
        target5_5 = np.load(os.path.join(self.target_path, target_file))['arr_0']
        return train5_5, target5_5
