import cv2
import numpy as np
from matplotlib import pyplot as plt
from tensorflow.keras.models import load_model
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from tqdm.keras import TqdmCallback
from tensorflow.keras.models import Model

from deeplearning_utils.file_sequence import FileGenerator

"""
Fichero encargado de proporcionar utilidades y herramientas para generar modelos de DeepLearning
utilizando tensorflow y Keras con los datos generados con la clase DeepLearningDataset.

Se genera como paquete independiente porque se ha utilizado dentro de Google Colab y es más sencillo
subir solamente un fichero.

Versión 2021-05-21
"""


def generar_combinaciones_2x2():
    """
    train representa las cabeceras de los nonogramas
    0101 -> los 2 primeros representan las filas y los 2 últimos las columnas
       0 1
    0 | | |
    1 | |x|

    target representa el tablero primero los valores de arriba de izquierda a 
    derecha y después los inferiores
    """
    train = [
        # Fila1, Fila 2, Columna 1, Columna 2
        [0, 0, 0, 0],
        [0, 1, 0, 1],
        [0, 1, 1, 0],
        [0, 2, 1, 1],
        [1, 0, 0, 1],
        [1, 1, 0, 2],
        [1, 1, 1, 1],
        [1, 2, 1, 2],
        [1, 0, 1, 0],
        [1, 1, 1, 1],
        [1, 1, 2, 0],
        [1, 2, 2, 1],
        [2, 0, 1, 1],
        [2, 1, 1, 2],
        [2, 1, 2, 1],
        [2, 2, 2, 2]
    ]
    target = [
        [0, 0, 0, 0],
        [0, 0, 0, 1],
        [0, 0, 1, 0],
        [0, 0, 1, 1],
        [0, 1, 0, 0],
        [0, 1, 0, 1],
        [0, 1, 1, 0],
        [0, 1, 1, 1],
        [1, 0, 0, 0],
        [1, 0, 0, 1],
        [1, 0, 1, 0],
        [1, 0, 1, 1],
        [1, 1, 0, 0],
        [1, 1, 0, 1],
        [1, 1, 1, 0],
        [1, 1, 1, 1]
    ]
    return train, target


def mostrar_combinaciones_2x2():
    train, target = generar_combinaciones_2x2()

    fig = plt.figure(figsize=(20, 10))
    for i, (v, img) in enumerate(zip(train, target)):
        ax = fig.add_subplot(4, 4, i + 1)
        ax.imshow(cv2.resize(np.array(img, dtype=np.uint8).reshape(2, 2), (4, 4)), vmin=0, vmax=1, )
        ax.set_xticks(np.arange(-0.5, 4, 2), minor=True)
        ax.set_yticks(np.arange(-0.5, 4, 2), minor=True)

        # Gridlines based on minor ticks
        ax.grid(which='minor', color='w', linestyle='-', linewidth=2)
        ax.title.set_text(str(v))

    plt.tight_layout()
    plt.show()


def load_data(train_path: str = "./datos/combined/train_combined.npz",
              target_path: str = "./datos/combined/target_combined.npz"):
    train5_5 = np.load(train_path)['arr_0']
    target5_5 = np.load(target_path)['arr_0']
    return train5_5, target5_5


def predecir_primeros_n_tableros(train_dataset, target_dataset, modelo, number: int = 1000, batch_size: int = 1):
    predicciones = modelo.predict(train_dataset[0:number], batch_size=batch_size)
    predicciones_adaptadas = predicciones.round().astype(np.uint8)

    contador = 0
    for prediction, real in zip(predicciones_adaptadas, target_dataset[0:number]):
        valor = np.all(prediction == real)
        if valor:
            contador += 1

    return contador / number


def fit_model(model, num_model: int, epochs=200, patience=10, tqdm_enabled=1, verbose=0,
              path_models="./modelos/", name_models="modelo_"):
    callbacks = []

    if patience is not None:
        callbacks.append(EarlyStopping(monitor='val_loss', patience=patience))

    if tqdm_enabled is not None:
        verbose = 0
        callbacks.append(TqdmCallback(verbose=tqdm_enabled))

    callbacks.append(ModelCheckpoint(filepath=path_models + name_models + str(num_model) + ".h5", save_best_only=True,
                                     monitor='val_loss'))

    train_generator = FileGenerator('./datos')
    validation_generator = FileGenerator('./datos/')

    history = model.fit(x=train_generator, epochs=epochs, validation_data=validation_generator, verbose=verbose,
                        callbacks=callbacks)

    val_loss_np = np.array(history.history['val_loss'])

    val_loss = val_loss_np.min()
    val_accuracy = history.history['val_accuracy'][np.argmin(val_loss_np)]

    fichero_resultados = open(path_models + "results.txt", 'a')
    fichero_resultados.write("modelo {},{},{}\n".format(num_model, val_loss, val_accuracy))
    fichero_resultados.close()

    return history, val_loss, val_accuracy


def fit_and_metrics(modelo, n_model, train_dataset, target_dataset, epochs=200, patience=10):
    _, val_loss, val_accuracy = fit_model(modelo, n_model, epochs=epochs, patience=patience)
    print("Modelo {} - val_loss: {} - val_accuracy: {}".format(n_model, val_loss, val_accuracy))
    val_mil = predecir_primeros_n_tableros(train_dataset, target_dataset, cargar_modelo(n_model)) * 100
    print(
        "Modelo {} - % de los 1000 primeros tableros están completamente bien predichos: {}".format(n_model, val_mil))


def cargar_modelo(num_model: int, path_models="./modelos/",
                  name_models="modelo_") -> Model:
    return load_model(path_models + name_models + str(num_model) + ".h5")
