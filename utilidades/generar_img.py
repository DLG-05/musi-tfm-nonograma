import cv2
from matplotlib import pyplot as plt
import numpy as np

from estructura import Nonograma

img = cv2.imread('/datos/imagenes/icons_v2/apple_0_airplane.png', flags=cv2.IMREAD_GRAYSCALE)
img_rescaled = cv2.resize(img, (10, 10), interpolation=cv2.INTER_LINEAR)
# Se aplica canny
img_canny = cv2.Canny(img_rescaled, 100, 200)
n1 = Nonograma.generar_nonograma_desde_img(img_canny, 120)

# Se hace un filtrado manual
img_blanc = np.zeros_like(img_rescaled)
filtro = np.logical_and(img_rescaled > 127, img_rescaled < 250)
img_blanc[filtro] = 255
n2 = Nonograma.generar_nonograma_desde_img(img_blanc, 127)

# Otsu and inverted Otsu
ret2, th2 = cv2.threshold(img_rescaled, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
n3 = Nonograma.generar_nonograma_desde_img(th2, 127)
th3 = np.abs(th2.astype(int) - 255).astype(np.uint8)
n4 = Nonograma.generar_nonograma_desde_img(th3, 127)

fig,ax = plt.subplots(3,2, figsize=(6,8))
for axs, img, title in zip(ax.flatten()[:2], [img, img_rescaled], ['Original image', 'Rescaled image']):
    axs.imshow(img, cmap='gray')
    axs.set_title(title)
    axs.axis('off')
for axs, img, title in zip(ax.flatten()[2:], [n1,n2,n3,n4], ['Canny', 'First thresholding ', 'Otsu', 'Inverted Otsu']):
    axs.imshow(img.get_tablero(), cmap='gray')
    axs.set_title(title)
    axs.axis('off')
plt.show()