import numpy as np
from tqdm import tqdm

from algoritmos.backtracking_with_ai import BacktrackingVorazConIA
from deeplearning_utils.utilidades import load_data
from estructura import Nonograma, FilaColumna, Tipo


RUTA_CSV = "./datos/10x10/resultados_ai_backtracking.csv"
TRAIN_PATH = './datos/10x10/x_train_backtracking.npz'
TARGET_PATH = './datos/10x10/y_train_backtracking.npz'
IA_NPZ = './datos/10x10/resultados_ai_backtracking.npz'
BK_NPZ = "./datos/10x10/resultados_backtracking.npz"

VECES = 30


def convertir_row_of_dataset_into_nonogram(x, ancho, alto):
    x_conv = x.reshape((ancho+alto, -1))

    n = Nonograma(ancho=ancho, alto=alto)

    for i in range(alto):
        lista_valores_adaptados = []
        lista_valores_originales = x_conv[i]
        for valor in lista_valores_originales:
            if valor != 0:
                lista_valores_adaptados.append(valor)
        n.agregar(Tipo.FILA, i, FilaColumna(lista_valores_adaptados))

    for i in range(ancho):
        lista_valores_adaptados = []
        lista_valores_originales = x_conv[i+alto]
        for valor in lista_valores_originales:
            if valor != 0:
                lista_valores_adaptados.append(valor)
        n.agregar(Tipo.COLUMNA, i, FilaColumna(lista_valores_adaptados))
    n.calcular_cuadros_a_marcar()
    return n


def info_backtracking_to_file(n_tablero, iteration, info, tiempo):
    with open(RUTA_CSV, "a") as file_object:
        for valor, prueba, n_combinaciones, ia_enabled in info:
            mensaje = "{};{};{};{};{};{};{}\n".format(n_tablero, iteration, ia_enabled, valor, prueba, n_combinaciones,
                                                      tiempo)
            file_object.write(mensaje)


x_train, y_train = load_data(train_path=TRAIN_PATH, target_path=TARGET_PATH)

tiempo_ai = np.zeros((len(x_train), VECES))
tiempo_back = np.zeros((len(x_train), VECES))

for i, x in enumerate(tqdm(x_train)):
    for j in range(VECES):
        if j % 2 == 0:
            # Backtracking IA
            n = convertir_row_of_dataset_into_nonogram(x, 10, 10)
            al = BacktrackingVorazConIA(n, enable_prints=False)
            al.run()
            tiempo_ai[i, j] = al.time
            info_backtracking_to_file(i, j, al.info_backtracking, al.time)
            # Backtracking normal
            n = convertir_row_of_dataset_into_nonogram(x, 10, 10)
            al = BacktrackingVorazConIA(n, ia_enabled=False, enable_prints=False)
            al.run()
            info_backtracking_to_file(i, j, al.info_backtracking, al.time)
            tiempo_back[i, j] = al.time
        else:
            # Backtracking normal
            n = convertir_row_of_dataset_into_nonogram(x, 10, 10)
            al = BacktrackingVorazConIA(n, ia_enabled=False, enable_prints=False)
            al.run()
            info_backtracking_to_file(i, j, al.info_backtracking, al.time)
            tiempo_back[i, j] = al.time

            # Backtracking IA
            n = convertir_row_of_dataset_into_nonogram(x, 10, 10)
            al = BacktrackingVorazConIA(n, enable_prints=False)
            al.run()
            info_backtracking_to_file(i, j, al.info_backtracking, al.time)
            tiempo_ai[i, j] = al.time

tiempo_ai_mean = tiempo_ai.mean()
tiempo_ai_std = tiempo_ai.std()

tiempo_back_mean = tiempo_back.mean()
tiempo_back_std = tiempo_back.std()

np.savez(IA_NPZ, tiempo_ai)
np.savez(BK_NPZ, tiempo_back)

print("tiempo medio ai: {} std: {}".format(tiempo_ai_mean, tiempo_ai_std))
print("tiempo medio back: {} std: {}".format(tiempo_back_mean, tiempo_back_std))
