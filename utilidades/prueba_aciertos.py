import numpy as np
from tqdm import tqdm

from deeplearning_utils.utilidades import load_data, cargar_modelo
from estructura import Nonograma, FilaColumna, Tipo


"""
Fichero que calcula cuantos tableros acierta a la primera un modelo de dl con un dataset concreto
"""

def convertir_row_of_dataset_into_nonogram(x, ancho, alto):
    x_conv = x.reshape((ancho+alto, -1))

    n = Nonograma(ancho=ancho, alto=alto)

    for i in range(alto):
        lista_valores_adaptados = []
        lista_valores_originales = x_conv[i]
        for valor in lista_valores_originales:
            if valor != 0:
                lista_valores_adaptados.append(valor)
        n.agregar(Tipo.FILA, i, FilaColumna(lista_valores_adaptados))

    for i in range(ancho):
        lista_valores_adaptados = []
        lista_valores_originales = x_conv[i+alto]
        for valor in lista_valores_originales:
            if valor != 0:
                lista_valores_adaptados.append(valor)
        n.agregar(Tipo.COLUMNA, i, FilaColumna(lista_valores_adaptados))
    n.calcular_cuadros_a_marcar()
    return n


def predecir(p, modelo) -> Nonograma:
    cabeceras = p.get_cabeceras_con_padding_y_concatenadas()
    nuevo_tablero = modelo.predict(np.array([cabeceras]))
    size_of_tablero = (p.get_alto_tablero(), p.get_ancho_tablero())
    tablero_int = nuevo_tablero[0].astype(np.uint8).reshape(size_of_tablero)
    tablero = Nonograma.KEYS[tablero_int]
    nuevo_nonograma = p.copiar_asignando_nuevo_tablero(tablero)
    return nuevo_nonograma


x_train, y_train = load_data(train_path='./datos/x_train_backtracking.npz',
                             target_path='./datos/y_train_backtracking.npz')

modelo = cargar_modelo(12, '../modelos/', name_models="modelo10x10_")

aciertos_a_la_primera = 0
for i, x in enumerate(tqdm(x_train)):
    tablero = convertir_row_of_dataset_into_nonogram(x, 10, 10)
    tablero_p = predecir(tablero, modelo)
    if tablero_p.is_solution():
        aciertos_a_la_primera += 1

print(aciertos_a_la_primera)