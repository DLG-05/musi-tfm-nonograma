from algoritmos.algorithm_detect_back import DetectBacktracking
from deeplearning_utils.utilidades import load_data
from estructura import Nonograma, Tipo, FilaColumna
import numpy as np
from tqdm import tqdm
from os import listdir
from os.path import join, isfile

"""
Script que se encarga de producir un dataset donde los tableros tienen la necesidad de utilizar backtracking
"""

INICIO = 0
FINAL = 15274  # 200  # 361094 # 15274
SIZE_NONOGRAM = 15
PATH_OUTPUT = "./datos/15x15/backtracking/"
OUTPUT_X = "test/x/"
OUTPUT_Y = "test/y/"

x, y = load_data(train_path='./datos/{}x{}/x_test_15x15_ok.npz'.format(SIZE_NONOGRAM, SIZE_NONOGRAM),
                 target_path='./datos/{}x{}/y_test_15x15_ok.npz'.format(SIZE_NONOGRAM, SIZE_NONOGRAM))

print(x.shape)


def convertir_row_of_dataset_into_nonogram(x, ancho, alto):
    x_conv = x.reshape((ancho+alto, -1))

    n = Nonograma(ancho=ancho, alto=alto)

    for i in range(alto):
        lista_valores_adaptados = []
        lista_valores_originales = x_conv[i]
        for valor in lista_valores_originales:
            if valor != 0:
                lista_valores_adaptados.append(valor)
        n.agregar(Tipo.FILA, i, FilaColumna(lista_valores_adaptados))

    for i in range(ancho):
        lista_valores_adaptados = []
        lista_valores_originales = x_conv[i+alto]
        for valor in lista_valores_originales:
            if valor != 0:
                lista_valores_adaptados.append(valor)
        n.agregar(Tipo.COLUMNA, i, FilaColumna(lista_valores_adaptados))
    n.calcular_cuadros_a_marcar()
    return n


def buscar_backtracking():
    # list_of_index_with_backtracking = []
    x_reducida = x[INICIO:FINAL]
    y_reducida = y[INICIO:FINAL]
    for j, t in enumerate(tqdm(x_reducida)):
        n = convertir_row_of_dataset_into_nonogram(t, SIZE_NONOGRAM, SIZE_NONOGRAM)
        al = DetectBacktracking(n)
        al.run()
        if al.use_backtracking:
            np.savez(join(join(PATH_OUTPUT, OUTPUT_X), "{}.npz".format(j + INICIO)), x_reducida[j])
            np.savez(join(join(PATH_OUTPUT, OUTPUT_Y), "{}.npz".format(j + INICIO)), y_reducida[j])
            # list_of_index_with_backtracking.append(j)

    # print(len(list_of_index_with_backtracking))


def unir_ficheros(path, output_path):
    lista = []
    ficheros = [f for f in listdir(path) if isfile(join(path, f))]

    for f in ficheros:
        tablero = np.load(join(path, f))['arr_0']
        lista.append(tablero)

    array_tableros = np.array(lista)
    np.savez(output_path, array_tableros)


if __name__ == '__main__':
    buscar_backtracking()
    unir_ficheros(join(PATH_OUTPUT, OUTPUT_X),
                  join(join(PATH_OUTPUT, OUTPUT_X), 'unido/x_test_backtracking_15x15.npz'))
    unir_ficheros(join(PATH_OUTPUT, OUTPUT_Y),
                  join(join(PATH_OUTPUT, OUTPUT_Y), 'unido/y_test_backtracking_15x15.npz'))
