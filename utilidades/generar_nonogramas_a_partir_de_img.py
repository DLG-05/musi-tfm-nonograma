from os import listdir
from os.path import isfile, join
from matplotlib import pyplot as plt
import cv2
import numpy as np
# mypath = './datos/icons/png'
mypath = './datos/icons_v2'

onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

img_parcial = onlyfiles[0:9]


plt.figure(figsize=(20, 20))

for i, path in enumerate(img_parcial):
    plt.subplot(9, 8, (i*8)+1)
    img = cv2.imread(join(mypath, path), flags=cv2.IMREAD_GRAYSCALE)
    print(join(mypath, path))
    print(img.shape)
    print(img.dtype)
    plt.imshow(img, cmap='gray', vmin=0, vmax=255)
    plt.subplot(9, 8, (i*8)+2)
    img_rescaled = cv2.resize(img, (10, 10), interpolation=cv2.INTER_LINEAR)
    plt.imshow(img_rescaled, cmap='gray', vmin=0, vmax=255)
    img_canny = cv2.Canny(img_rescaled, 100, 200)
    plt.subplot(9, 8, (i*8)+3)
    plt.imshow(img_canny, cmap='gray', vmin=0, vmax=255)
    img_canny_2 = cv2.Canny(img, 100, 200)
    plt.subplot(9, 8, (i*8)+4)
    plt.imshow(img_canny_2, cmap='gray')
    img_rescaled_canny = cv2.resize(img_canny_2, (10, 10), interpolation=cv2.INTER_LINEAR)
    plt.subplot(9, 8, (i*8)+5)
    plt.imshow(img_rescaled_canny, cmap='gray')

    img_blanc = np.zeros_like(img_rescaled)
    filtro = np.logical_and(img_rescaled > 127, img_rescaled < 250)
    img_blanc[filtro] = 255
    plt.subplot(9, 8, (i*8)+6)
    plt.imshow(img_blanc, cmap='gray')

    ret2, th2 = cv2.threshold(img_rescaled, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    plt.subplot(9, 8, (i * 8) + 7)
    plt.imshow(th2, cmap='gray')

    plt.subplot(9, 8, (i*8)+8)
    plt.imshow(abs(th2 - 255), cmap='gray')

plt.show()

