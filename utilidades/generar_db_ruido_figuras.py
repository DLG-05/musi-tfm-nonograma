import numpy as np
from tqdm import tqdm

from estructura import Nonograma, FilaColumna


def generar_base_datos_ruido(path_db, path_y_db, size=300000):
    db = np.zeros((size, (FilaColumna.get_size_of_fila_columna_with_padding(15) * 15) * 2),
                  dtype=np.uint8)
    db_y = np.zeros((size, 15 * 15), dtype=np.uint8)

    for i in tqdm(range(size)):
        if i % 3 == 0:
            # Generar tableros de ruido completamente aleatorio
            nonograma = Nonograma.generar_nonograma_aleatorio(15, 15, 0.5)
        else:
            nonograma = Nonograma.generar_nonograma_aleatorio_figuras(ancho=15, alto=15, max_lines=9, max_circles=7,
                                                                      max_squares=7)

        db[i, :] = nonograma.get_cabeceras_con_padding_y_concatenadas()
        db_y[i, :] = nonograma.get_tablero().flatten()

    np.savez_compressed(path_db, db)
    np.savez_compressed(path_y_db, db_y)


generar_base_datos_ruido('../datos/database_noise_15x15_v2.npz', '../datos/database_y_noise_15x15_v2.npz')
