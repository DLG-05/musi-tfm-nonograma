from tqdm import tqdm

from algoritmos.backtracking_with_ai_v3 import BacktrackingVorazConIAV3
from algoritmos.particles_algorithm import ParticleAlgorithm
from algoritmos.utilidades_ia import convertir_row_of_dataset_into_nonograma, predecir_usando_ia, cargar_modelo_manual
from deeplearning_utils.utilidades import load_data
from estructura import Nonograma, Contenido

SIZE_TABLERO = 10  # 15
PATH_DATASET_X = './datos/10x10/x_test_dataset.npz'  # './datos/15x15/x_test_15x15.npz'
PATH_DATASET_Y = './datos/10x10/y_test_dataset.npz'  # './datos/15x15/y_test_15x15.npz'
PATH_RESULTADOS = './datos/10x10/resultados/'  # './datos/15x15/resultados/'

modelo = cargar_modelo_manual(SIZE_TABLERO)


def calcular_errores(solution: Nonograma, tablero_a_predecir: Nonograma, dataset_y=None):
    if solution is None:
        solution = Nonograma(SIZE_TABLERO, SIZE_TABLERO,
                             tablero=Nonograma.KEYS[dataset_y.reshape(SIZE_TABLERO, SIZE_TABLERO)])
        solution.calcular_cuadros_a_marcar()
        # return -1, -1, -1, -1
    prediction = predecir_usando_ia(modelo, tablero_a_predecir)
    solution_tablero = solution.get_tablero()
    prediction_tablero = prediction.get_tablero()

    correctas = 0
    equivocadas = 0
    falsos_rellenados = 0
    falsos_cross = 0

    for idx_alto in range(solution.get_alto_tablero()):
        for idx_ancho in range(solution.get_ancho_tablero()):
            if solution_tablero[idx_alto, idx_ancho] == prediction_tablero[idx_alto, idx_ancho]:
                correctas += 1
            elif solution_tablero[idx_alto, idx_ancho] == Contenido.CROSS and \
                    prediction_tablero[idx_alto, idx_ancho] == Contenido.FULL:
                falsos_rellenados += 1
                equivocadas += 1
            elif solution_tablero[idx_alto, idx_ancho] == Contenido.FULL and \
                    prediction_tablero[idx_alto, idx_ancho] == Contenido.CROSS:
                falsos_cross += 1
                equivocadas += 1
    return correctas, equivocadas, falsos_rellenados, falsos_cross


x, y = load_data(train_path=PATH_DATASET_X, target_path=PATH_DATASET_Y)

j = 1

while True:
    PATH_OUTPUT_AL1 = PATH_RESULTADOS + str(j) + '_al1.csv'
    PATH_OUTPUT_AL2 = PATH_RESULTADOS + str(j) + '_al2.csv'
    PATH_OUTPUT_AL3 = PATH_RESULTADOS + str(j) + '_al3.csv'
    PATH_OUTPUT_AL4 = PATH_RESULTADOS + str(j) + '_al4.csv'
    # PATH_OUTPUT_AL5 = PATH_RESULTADOS + str(j) + '_al5.csv'

    j += 1

    with open(PATH_OUTPUT_AL1, mode='w', encoding='utf-8') as f:
        f.write("tablero;tiempo;iteraciones;veces_backtracking\n")
    with open(PATH_OUTPUT_AL2, mode='w', encoding='utf-8') as f:
        f.write("tablero;tiempo;ia_a_la_primera;iteraciones;veces_backtracking;correctas;errores;"
                "falsos_rellenados;falsos_cross\n")
    with open(PATH_OUTPUT_AL3, mode='w', encoding='utf-8') as f:
        f.write("tablero;tiempo;ia_a_la_primera;filas_borradas;columnas_borradas;filas_columnas_borradas;"
                "celdas_borradas;celdas_mantenidas;"
                "borrado_completo;veces_backtracking_pre_borrado_total;veces_backtracking_post_borrado_total;"
                "iteraciones_pre_borrado_total;iteraciones_post_borrado_total;"
                "correctas;errores;falsos_rellenados;falsos_cross\n")
    with open(PATH_OUTPUT_AL4, mode='w', encoding='utf-8') as f:
        f.write("tablero;tiempo;ia_a_la_primera;filas_borradas;columnas_borradas;filas_columnas_borradas;"
                "celdas_borradas;celdas_mantenidas;"
                "borrado_completo;veces_backtracking_pre_borrado_total;veces_backtracking_post_borrado_total;"
                "iteraciones_pre_borrado_total;iteraciones_post_borrado_total;"
                "correctas;errores;falsos_rellenados;falsos_cross\n")
    # with open(PATH_OUTPUT_AL5, mode='w', encoding='utf-8') as f:
    #     f.write("tablero;tiempo;hay_solución;iteración;ia_a_la_primera;correctas;errores;"
    #             "falsos_rellenados;falsos_cross\n")

    for i, tablero in enumerate(tqdm(x)):
        n = convertir_row_of_dataset_into_nonograma(tablero, SIZE_TABLERO, SIZE_TABLERO)

        al1 = BacktrackingVorazConIAV3(tablero=n, enable_prints=False, ia_enabled=False, intuition=False,
                                       using_prediction=False)
        al1.run()
        al1_tiempo = al1.time
        al1_veces_backtracking = al1.veces_backtracking
        al1_iteraciones = al1.iteraciones
        with open(PATH_OUTPUT_AL1, mode='a', encoding='utf-8') as f:
            f.write("{};{};{};{}\n".format(i, al1_tiempo, al1_iteraciones, al1_veces_backtracking))

        al2 = BacktrackingVorazConIAV3(tablero=n, enable_prints=False, ia_enabled=True, intuition=True,
                                       using_prediction=False)
        al2.run()
        al2_tiempo = al2.time
        al2_solution = al2.solution
        al2_ia_a_la_primera = al2.ia_a_la_primera
        al2_iteraciones = al2.iteraciones
        al2_veces_backtracking = al2.veces_backtracking
        al2_correctas, al2_equivocadas, al2_falsos_rellenados, al2_falsos_cross = calcular_errores(al2_solution, n,
                                                                                                   y[i])
        with open(PATH_OUTPUT_AL2, mode='a', encoding='utf-8') as f:
            f.write("{};{};{};{};{};{};{};{};{}\n".format(i, al2_tiempo, al2_ia_a_la_primera, al2_iteraciones,
                                                          al2.veces_backtracking, al2_correctas, al2_equivocadas,
                                                          al2_falsos_rellenados, al2_falsos_cross))

        al3 = BacktrackingVorazConIAV3(tablero=n, enable_prints=False, ia_enabled=True, intuition=True,
                                       using_prediction=True)
        al3.run()
        al3_tiempo = al3.time
        al3_solution = al3.solution
        al3_ia_a_la_primera = al3.ia_a_la_primera
        al3_filas_columnas_borradas = al3.filas_columnas_borradas
        al3_filas_borradas = al3.filas_borradas
        al3_columnas_borradas = al3.columnas_borradas
        al3_celdas_borradas = al3.celdas_borradas
        al3_celdas_mantenidas = al3.celdas_mantenidas
        al3_borrado_completo = al3.borrado_completo
        al3_v_backtracking_pre_borrado_total = al3.veces_backtracking_pre_borrado_total
        al3_v_backtracking_post_borrado_total = al3.veces_backtracking_post_borrado_total
        al3_iteraciones_pre_borrado_total = al3.iteraciones_pre_borrado
        al3_iteraciones_post_borrado_total = al3.iteraciones_post_borrado
        al3_correctas, al3_equivocadas, al3_falsos_rellenados, al3_falsos_cross = calcular_errores(al3_solution, n,
                                                                                                   y[i])

        with open(PATH_OUTPUT_AL3, mode='a', encoding='utf-8') as f:
            f.write("{};{};{};{};{};{};{};{};{};{};{};{};{};{};{};{};{}\n".format(i, al3_tiempo, al3_ia_a_la_primera,
                                                                                  al3_filas_borradas,
                                                                                  al3_columnas_borradas,
                                                                                  al3_filas_columnas_borradas,
                                                                                  al3_celdas_borradas,
                                                                                  al3_celdas_mantenidas,
                                                                                  al3_borrado_completo,
                                                                                  al3_v_backtracking_pre_borrado_total,
                                                                                  al3_v_backtracking_post_borrado_total,
                                                                                  al3_iteraciones_pre_borrado_total,
                                                                                  al3_iteraciones_post_borrado_total,
                                                                                  al3_correctas, al3_equivocadas,
                                                                                  al3_falsos_rellenados,
                                                                                  al3_falsos_cross))

        al4 = BacktrackingVorazConIAV3(tablero=n, enable_prints=False, ia_enabled=True, intuition=False,
                                       using_prediction=True)
        al4.run()
        al4_tiempo = al4.time
        al4_solution = al4.solution
        al4_ia_a_la_primera = al4.ia_a_la_primera
        al4_filas_borradas = al4.filas_borradas
        al4_celdas_borradas = al4.celdas_borradas
        al4_celdas_mantenidas = al4.celdas_mantenidas
        al4_columnas_borradas = al4.columnas_borradas
        al4_filas_columnas_borradas = al4.filas_columnas_borradas
        al4_borrado_completo = al4.borrado_completo
        al4_v_backtracking_pre_borrado_total = al4.veces_backtracking_pre_borrado_total
        al4_v_backtracking_post_borrado_total = al4.veces_backtracking_post_borrado_total
        al4_iteraciones_pre_borrado_total = al4.iteraciones_pre_borrado
        al4_iteraciones_post_borrado_total = al4.iteraciones_post_borrado
        al4_correctas, al4_equivocadas, al4_falsos_rellenados, al4_falsos_cross = calcular_errores(al4_solution, n,
                                                                                                   y[i])

        with open(PATH_OUTPUT_AL4, mode='a', encoding='utf-8') as f:
            f.write("{};{};{};{};{};{};{};{};{};{};{};{};{};{};{};{};{}\n".format(i, al4_tiempo, al4_ia_a_la_primera,
                                                                                  al4_filas_borradas,
                                                                                  al4_columnas_borradas,
                                                                                  al4_filas_columnas_borradas,
                                                                                  al4_celdas_borradas,
                                                                                  al4_celdas_mantenidas,
                                                                                  al4_borrado_completo,
                                                                                  al4_v_backtracking_pre_borrado_total,
                                                                                  al4_v_backtracking_post_borrado_total,
                                                                                  al4_iteraciones_pre_borrado_total,
                                                                                  al4_iteraciones_post_borrado_total,
                                                                                  al4_correctas, al4_equivocadas,
                                                                                  al4_falsos_rellenados,
                                                                                  al4_falsos_cross))

        # al5 = ParticleAlgorithm(n, size_of_population=1000, iteraciones=100, enable_tqdm=False)
        # al5.run()
        # al5_tiempo = al5.time
        # al5_hay_solution = al5.hay_solution
        # al5_solution = al5.solution
        # al5_iteration = al5.iteration
        # al5_ia_a_la_primera = al5.ia_a_la_primera
        # al5_correctas, al5_equivocadas, al5_falsos_rellenados, al5_falsos_cross = calcular_errores(al5_solution, n,
        #                                                                                            y[i])
        #
        # with open(PATH_OUTPUT_AL5, mode='a', encoding='utf-8') as f:
        #     f.write("{};{};{};{};{};{};{};{};{}\n".format(i, al5_tiempo, al5_hay_solution, al5_iteration,
        #                                                   al5_ia_a_la_primera, al5_correctas, al5_equivocadas,
        #                                                   al5_falsos_rellenados, al5_falsos_cross))
