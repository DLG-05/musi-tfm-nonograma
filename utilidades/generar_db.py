import os
from os import listdir
from os.path import isfile, join
from matplotlib import pyplot as plt
import cv2
import numpy as np
from tqdm import tqdm

from estructura import Nonograma, FilaColumna

path1 = './datos/icons/png'
path2 = './datos/icons_v2'

print(os.getcwd())

files1 = [f for f in listdir(path1) if isfile(join(path1, f))]
files2 = [f for f in listdir(path2) if isfile(join(path2, f))]


def generar_base_datos(mypath1, onlyfiles1, path_db, path_y_db):
    db = np.zeros((len(onlyfiles1) * 4, (FilaColumna.get_size_of_fila_columna_with_padding(15)*15)*2), dtype=np.uint8)
    db_y = np.zeros((len(onlyfiles1) * 4, 15*15), dtype=np.uint8)

    for i, path in enumerate(tqdm(onlyfiles1)):
        try:
            img = cv2.imread(join(mypath1, path), flags=cv2.IMREAD_GRAYSCALE)
            img_rescaled = cv2.resize(img, (15, 15), interpolation=cv2.INTER_LINEAR)
            # Se aplica canny
            img_canny = cv2.Canny(img_rescaled, 100, 200)
            n1 = Nonograma.generar_nonograma_desde_img(img_canny, 120)

            # Se hace un filtrado manual
            img_blanc = np.zeros_like(img_rescaled)
            filtro = np.logical_and(img_rescaled > 127, img_rescaled < 250)
            img_blanc[filtro] = 255
            n2 = Nonograma.generar_nonograma_desde_img(img_blanc, 127)

            # Otsu and inverted Otsu
            ret2, th2 = cv2.threshold(img_rescaled, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
            n3 = Nonograma.generar_nonograma_desde_img(th2, 127)
            th3 = np.abs(th2.astype(int) - 255).astype(np.uint8)
            n4 = Nonograma.generar_nonograma_desde_img(th3, 127)

            db[(i * 4) + 0, :] = n1.get_cabeceras_con_padding_y_concatenadas()
            db[(i * 4) + 1, :] = n2.get_cabeceras_con_padding_y_concatenadas()
            db[(i * 4) + 2, :] = n3.get_cabeceras_con_padding_y_concatenadas()
            db[(i * 4) + 3, :] = n4.get_cabeceras_con_padding_y_concatenadas()

            db_y[(i * 4) + 0, :] = n1.get_tablero().flatten()
            db_y[(i * 4) + 1, :] = n2.get_tablero().flatten()
            db_y[(i * 4) + 2, :] = n3.get_tablero().flatten()
            db_y[(i * 4) + 3, :] = n4.get_tablero().flatten()
        except:
            print(path)
    np.savez_compressed(path_db, db)
    np.savez_compressed(path_y_db, db_y)


def concatenar_bases_de_datos(mypath1, mypath2, output_path):
    a = np.load(mypath1)['arr_0']
    b = np.load(mypath2)['arr_0']
    c = np.concatenate((a, b), axis=0)
    np.savez_compressed(output_path, c)


def visualizar_algunos_elementos(mypath1):
    a = np.load(mypath1)['arr_0']
    plt.figure()
    for i in range(0, 12):
        plt.subplot(3, 4, i + 1)
        plt.imshow(a[i+20].reshape((15, 15)), cmap='gray')
    plt.show()


generar_base_datos(path1, files1, './datos/database_icons_15x15.npz', './datos/database_y_icons_15x15.npz')
generar_base_datos(path2, files2, './datos/database_icons_v2_15x15.npz', './datos/database_y_icons_v2_15x15.npz')
concatenar_bases_de_datos('./datos/database_icons_15x15.npz', './datos/database_icons_v2_15x15.npz',
                          './datos/database_icons_all_15x15.npz')
concatenar_bases_de_datos('./datos/database_y_icons_15x15.npz', './datos/database_y_icons_v2_15x15.npz',
                          './datos/database_y_icons_all_15x15.npz')

visualizar_algunos_elementos('./datos/database_y_icons_all_15x15.npz')
