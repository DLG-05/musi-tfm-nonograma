from os import listdir
from os.path import isfile, join
from PIL import Image
import imagehash as ih
import shutil

from tqdm import tqdm

mypath = './datos/icons'
my_new_path = '../datos/icons/repetidos'
ico_files = './datos/icons/png'

onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

diccionario = {}

for file in tqdm(onlyfiles):
    try:
        img = Image.open(join(mypath, file))
        hash_value = ih.average_hash(img)
        if hash_value not in diccionario:
            diccionario[hash_value] = True
            if file.endswith('ico'):
                path = join(ico_files, file[:-3] + 'png')
                # print(path)
                img.save(path, 'png')
                # img.close()
                # shutil.move(join(mypath, file), join(ico_files, file))

        else:
            shutil.move(join(mypath, file), join(my_new_path, file))
    except ValueError as e:
        print(file)
