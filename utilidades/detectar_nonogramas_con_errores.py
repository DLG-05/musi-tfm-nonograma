import numpy as np
from tqdm import tqdm

from deeplearning_utils.utilidades import load_data
from deeplearning_utils.utilidades_10 import cargar_modelo
from estructura import Nonograma, Contenido

modelo = cargar_modelo(16, path_models='./modelos/')
x, y = load_data(train_path='./datos/10x10/x_test_dataset.npz', target_path='./datos/10x10/y_test_dataset.npz')

predicciones = modelo.predict(x, batch_size=1000)
predicciones_adaptadas = predicciones.round().astype(np.uint8)

ficheros_errores = {
    1: "./datos/10x10/tableros_con_1_error.txt",
    2: './datos/10x10/tableros_con_2_errores.txt',
    3: './datos/10x10/tableros_con_3_errores.txt',
    4: './datos/10x10/tableros_con_4_errores.txt',
    5: './datos/10x10/tableros_con_5_errores.txt',
    6: './datos/10x10/tableros_con_6_errores.txt'
}

for prediction, real in tqdm(zip(predicciones_adaptadas, y), total=len(y)):
    n_err = len(np.where(prediction != real)[0])
    prediction = Nonograma.KEYS[prediction].astype(Contenido)
    real = Nonograma.KEYS[real].astype(Contenido)
    if 0 < n_err < 6:
        p_r = prediction.reshape(10, 10)
        r_r = real.reshape(10, 10)
        n_p_r = Nonograma(tablero=p_r, ancho=10, alto=10)
        n_r_r = Nonograma(tablero=r_r, ancho=10, alto=10)
        with open(ficheros_errores[n_err], 'a', encoding='utf-8') as f:
            f.write("=========== Nº Errores: {} =============\n".format(n_err))
            f.write("Tablero real:\n")

            f.write(str(n_r_r))
            f.write(str('\n'))
            f.write("Tablero predicho:\n")
            f.write(str(n_p_r))
            f.write(str('\n'))
            f.write("========================================\n")
