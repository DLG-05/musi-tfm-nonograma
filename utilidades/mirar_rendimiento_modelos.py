from deeplearning_utils.utilidades import load_data
import deeplearning_utils.utilidades_10 as u
import numpy as np
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
import pandas as pd

modelo = u.cargar_modelo(16)

train10x10, target10x10 = load_data(train_path='../datos/database_icons_all.npz',
                                    target_path='../datos/database_y_icons_all.npz')

x_train, x_test, y_train, y_test = train_test_split(train10x10, target10x10,
                                                    test_size=0.20,
                                                    random_state=42)
predicciones = modelo.predict(x_test)
p2 = predicciones.round().astype(np.uint8)

contador = np.zeros(100)
for prediccion, real in zip(p2, y_test):
    valor = len(np.where(prediccion != real)[0])
    contador[valor] = contador[valor] + 1

rango = np.linspace(0, 99, 100)
plt.plot(rango, contador)
plt.show()
print()
